| author name         | ticket url                                        | sprint number | comment | action: ADDED/UPDATED/REMOVED |
|---------------------|---------------------------------------------------|---------------|---------|-------------------------------|
| Daniel Witkowski    | https://jira.simplprogramme.eu/browse/SIMPL-7905  | 12            |         | ADDED                         |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-10245 | 12            |         | ADDED                         |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-10252 | 11            |         |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6952  | 11            |         |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-9147  | 10            |         |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-5035  | 10            |         |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6946  | 8             |         |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-7077  | 8             |         |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6055  | 8             |         |                               |

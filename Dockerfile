#it is a copy of Dockerfile in fc-service-server, - reason for this duplication
#is that our gitlab pipeline requires dockerfile in root folder
FROM eclipse-temurin:17.0.10_7-jdk
COPY fc-service-server/target/fc-service-server-*.jar fc-service-server.jar
ENTRYPOINT ["java", "-jar","/fc-service-server.jar"]

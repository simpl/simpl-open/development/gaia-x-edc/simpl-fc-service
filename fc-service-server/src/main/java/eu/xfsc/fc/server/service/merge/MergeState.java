package eu.xfsc.fc.server.service.merge;

import eu.xfsc.fc.api.generated.model.Results;
import eu.xfsc.fc.server.config.QueryProperties;

import java.util.Map;

public interface MergeState {
  void merge(Results results, Map<String, Object> item, Results local);

  void mergeLocal(Results results, Results local, QueryProperties queryProps);
}
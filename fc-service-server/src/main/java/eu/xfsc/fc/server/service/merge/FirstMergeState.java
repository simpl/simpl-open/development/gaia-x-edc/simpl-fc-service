package eu.xfsc.fc.server.service.merge;

import eu.xfsc.fc.api.generated.model.Results;
import eu.xfsc.fc.server.config.QueryProperties;

import java.util.List;
import java.util.Map;

public class FirstMergeState implements MergeState {
  @Override
  public void merge(Results results, Map<String, Object> item, Results local) {
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> items = (List<Map<String, Object>>) item.get("items");
    if (items != null) {
      results.getItems().addAll(items);
    }
  }

  @Override
  public void mergeLocal(Results results, Results local, QueryProperties queryProps) {
    results.getItems().addAll(local.getItems());
  }
}
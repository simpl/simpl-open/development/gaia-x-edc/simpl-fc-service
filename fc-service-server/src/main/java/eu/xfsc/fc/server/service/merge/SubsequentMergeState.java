package eu.xfsc.fc.server.service.merge;

import eu.xfsc.fc.api.generated.model.Results;
import eu.xfsc.fc.server.config.QueryProperties;

import java.util.List;
import java.util.Map;

public class SubsequentMergeState implements MergeState {
  @Override
  public void merge(Results results, Map<String, Object> item, Results local) {
    results.addItemsItem(item);
  }

  @Override
  public void mergeLocal(Results results, Results local, QueryProperties queryProps) {
    results.addItemsItem(Map.of(
        "server", queryProps.getSelf(),
        "total", local.getTotalCount(),
        "items", local.getItems()
    ));
  }
}
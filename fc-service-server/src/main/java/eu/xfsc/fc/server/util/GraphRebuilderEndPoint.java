package eu.xfsc.fc.server.util;

import eu.xfsc.fc.core.util.GraphRebuilder;
import eu.xfsc.fc.server.model.GraphRebuildRequest;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.web.annotation.WebEndpoint;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Component
@WebEndpoint(id = "graph-rebuild")
@Validated
public class GraphRebuilderEndPoint {

  @Autowired
  private GraphRebuilder graphRebuilder;

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<String> startGraphRebuild(@RequestBody @Valid GraphRebuildRequest grRequest) {
    log.debug("startGraphRebuild.enter; got request: {}", grRequest);
    graphRebuilder.rebuildGraphDb(grRequest.getChunkCount(), grRequest.getChunkId(), grRequest.getThreads(), grRequest.getBatchSize());
    return ResponseEntity.ok("graph-rebuild started successfully");
  }
}

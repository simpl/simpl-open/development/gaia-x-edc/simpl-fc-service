package eu.xfsc.fc.server.service;

import java.util.Arrays;
import java.util.List;

import eu.xfsc.fc.core.dao.UserDao;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class RolesServiceTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private RolesService rolesService;

    @Test
    void getALlRolesShouldReturnStatus200AndBody() {
        // Given
        List<String> roles = Arrays.asList("ROLE_USER", "ROLE_ADMIN");
        when(userDao.getAllRoles()).thenReturn(roles);

        // When
        ResponseEntity<List<String>> response = rolesService.getAllRoles();

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).containsExactly("ROLE_USER", "ROLE_ADMIN");
    }
}
package eu.xfsc.fc.server.controller;

import eu.xfsc.fc.core.service.schemastore.SchemaStore;

import java.util.Objects;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase.DatabaseProvider;

import static eu.xfsc.fc.server.helper.FileReaderHelper.getMockFileDataAsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureEmbeddedDatabase(provider = DatabaseProvider.ZONKY)
class SchemaControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private SchemaStore schemaStore;

  @AfterEach
  public void storageSelfCleaning() {
    schemaStore.clear();
  }

  @Test
  void addSchemaShouldReturnSuccessResponse() throws Exception {
    ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/schemas")
            .content(getMockFileDataAsString("test-schema.ttl"))
            .contentType("application/rdf+xml")
    ).andExpect(status().isCreated());

    MvcResult result = resultActions.andReturn();
    String id = Objects.requireNonNull(result.getResponse().getHeader("location"))
        .replace("/schemas/", "");
    schemaStore.deleteSchema(id);
  }
}

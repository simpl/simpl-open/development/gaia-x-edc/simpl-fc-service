package eu.xfsc.fc.server.service;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import eu.xfsc.fc.api.generated.model.SelfDescription;
import eu.xfsc.fc.api.generated.model.SelfDescriptionStatus;
import eu.xfsc.fc.api.generated.model.SelfDescriptions;
import eu.xfsc.fc.core.exception.ConflictException;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.pojo.PaginatedResults;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.pojo.VerificationResultOffering;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.TrustFrameworkBase;
import eu.xfsc.fc.core.service.verification.VerificationService;
import eu.xfsc.fc.core.service.verification.VerificationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SelfDescriptionServiceTest {

    @Mock
    private VerificationService verificationService;

    @Mock
    private SelfDescriptionStore sdStorePublisher;

    @Mock
    private VerificationServiceImpl verificationServiceImpl;

    private SelfDescriptionService selfDescriptionService;

    //for some reasons @InjecMock did not inject verification service so i had to write this
    @BeforeEach
    void setup() {
        this.selfDescriptionService = new SelfDescriptionService(verificationService, sdStorePublisher, verificationServiceImpl);
    }

    @Test
    void readSelfDescriptionsShouldReturnSelfDescriptionsWithStatus200() {
        final var selfDescriptionMetaData = new SelfDescriptionMetadata();
        selfDescriptionMetaData.setSelfDescription(new ContentAccessorDirect("lala"));
        PaginatedResults<SelfDescriptionMetadata> paginatedResults = new PaginatedResults<>(List.of(selfDescriptionMetaData));
        when(sdStorePublisher.getByFilter(any(), eq(true), eq(true))).thenReturn(paginatedResults);

        ResponseEntity<SelfDescriptions> response = selfDescriptionService.readSelfDescriptions(null, null,
                null, null, null, null, null, true, true, 0, 10);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void readSelfDescriptionByIdShouldReturnOkWithBody() {
        final var contentAccessor = new ContentAccessorDirect("lalala");

        final var selfDescriptionMetadata = new SelfDescriptionMetadata();
        selfDescriptionMetadata.setSelfDescription(contentAccessor);

        when(sdStorePublisher.getById(anyString()))
                .thenReturn(selfDescriptionMetadata);

        final var result = selfDescriptionService.readSelfDescriptionById("id");

        assertThat(result.getStatusCode())
                .isEqualTo(HttpStatus.OK);

        assertThat(result.getBody())
                .isEqualTo("lalala");
    }

    @Test
    void deleteSelfDescriptionShouldDeleteAndReturnOk() {
        String selfDescriptionHash = "someHash";
        String issuer = "issuer";
        SelfDescriptionMetadata metadata = new SelfDescriptionMetadata();
        metadata.setIssuer(issuer);
        when(sdStorePublisher.getByHash(selfDescriptionHash)).thenReturn(metadata);

        ResponseEntity<Void> response = selfDescriptionService.deleteSelfDescription(selfDescriptionHash);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(sdStorePublisher, times(1)).deleteSelfDescription(selfDescriptionHash);
    }

    @Test
    void updateSelfDescriptionShouldUpdateAndReturnOk() {
        String selfDescriptionHash = "someHash";
        SelfDescriptionMetadata metadata = mock(SelfDescriptionMetadata.class);
        when(sdStorePublisher.getByHash(selfDescriptionHash)).thenReturn(metadata);
        when(metadata.getStatus()).thenReturn(SelfDescriptionStatus.ACTIVE);

        ResponseEntity<SelfDescription> response = selfDescriptionService.updateSelfDescription(selfDescriptionHash);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(sdStorePublisher, times(1)).changeLifeCycleStatus(any(), any());
    }

    @Test
    void updateSelfDescriptionShouldThrowConflictExceptionIfNotActive() {
        String selfDescriptionHash = "someHash";

        SelfDescriptionMetadata metadata = mock(SelfDescriptionMetadata.class);
        when(sdStorePublisher.getByHash(selfDescriptionHash)).thenReturn(metadata);
        when(metadata.getStatus()).thenReturn(SelfDescriptionStatus.REVOKED);

        assertThatThrownBy(() -> selfDescriptionService.updateSelfDescription(selfDescriptionHash))
                .isInstanceOf(ConflictException.class);

        verify(sdStorePublisher, never()).changeLifeCycleStatus(any(), any());
    }

    @Test
    void updateServiceOfferingBaseClassShouldUpdateBaseClassName() {
        String baseClassName = "newBaseClass";
        Map<TrustFrameworkBase, String> baseClassUris = mock(Map.class);
        when(verificationServiceImpl.getTrustFrameworkBaseClassUris()).thenReturn(baseClassUris);

        ResponseEntity<Void> response = selfDescriptionService.updateServiceOfferingBaseClass(baseClassName);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(baseClassUris, times(1)).put(TrustFrameworkBase.SERVICE_OFFERING, baseClassName);
    }

    @Test
    void addSelfDescriptionShouldStoreAndReturnCreated() {
        String selfDescription = "someDescription";

        VerificationResultOffering verificationResult = new VerificationResultOffering(Instant.now(), "live", "issuer", Instant.now(), "id", List.of(), List.of());
        when(verificationService.verifyOfferingSelfDescription(any())).thenReturn(verificationResult);

        ResponseEntity<SelfDescription> response = selfDescriptionService.addSelfDescription(selfDescription);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        verify(sdStorePublisher, times(1)).storeSelfDescription(any(SelfDescriptionMetadata.class), eq(verificationResult));
    }
}
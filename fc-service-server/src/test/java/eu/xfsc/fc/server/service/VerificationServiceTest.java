package eu.xfsc.fc.server.service;

import java.io.IOException;

import eu.xfsc.fc.core.exception.ServerException;
import eu.xfsc.fc.core.pojo.VerificationResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VerificationServiceTest {

    @Mock
    private eu.xfsc.fc.core.service.verification.VerificationService verificationService;

    @Mock
    private ResourceLoader resourceLoader;

    @InjectMocks
    private VerificationService service;

    @Test
    void verifyShouldReturnVerificationResultWhenCalledWithValidInput() {
        // Given
        String body = "{\"key\":\"value\"}";
        Boolean verifySemantics = true;
        Boolean verifySchema = true;
        Boolean verifyVPSignature = true;
        Boolean verifyVCSignature = true;
        VerificationResult mockResult = mock(VerificationResult.class);

        when(verificationService.verifySelfDescription(any(), anyBoolean(), anyBoolean(), anyBoolean(), anyBoolean()))
                .thenReturn(mockResult);

        // When
        ResponseEntity<eu.xfsc.fc.api.generated.model.VerificationResult> response = service
                .verify(verifySemantics, verifySchema, verifyVPSignature, verifyVCSignature, body);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(mockResult);
    }

    @Test
    void verifyPageShouldReturnHtmlPageWhenPageLoadsSuccessfully() throws IOException {
        // Given
        Resource mockResource = mock(Resource.class);
        String htmlContent = "<html><body>Verification Page</body></html>";

        when(resourceLoader.getResource("classpath:static/verification.html")).thenReturn(mockResource);
        when(mockResource.getInputStream()).thenReturn(new java.io.ByteArrayInputStream(htmlContent.getBytes()));

        // When
        ResponseEntity<String> response = service.verifyPage();

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo("text/html");
        assertThat(response.getBody()).isEqualTo(htmlContent);
    }

    @Test
    void verifyPageShouldThrowServerExceptionWhenIOExceptionOccurs() throws IOException {
        // Given
        Resource mockResource = mock(Resource.class);
        when(resourceLoader.getResource("classpath:static/verification.html")).thenReturn(mockResource);
        when(mockResource.getInputStream()).thenThrow(new IOException("File not found"));

        // When / Then
        assertThatThrownBy(() -> service.verifyPage())
                .isInstanceOf(ServerException.class)
                .hasMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
}
package eu.xfsc.fc.server.service;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import eu.xfsc.fc.api.generated.model.OntologySchema;
import eu.xfsc.fc.core.exception.ClientException;
import eu.xfsc.fc.core.exception.NotFoundException;
import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.service.schemastore.SchemaStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SchemasServiceTest {

    @Mock
    private SchemaStore schemaStore;

    @InjectMocks
    private SchemasService schemasService;

    @Test
    void getSchemaShouldReturn200WithBody() {
        // Given
        String id = "schemaId";
        String content = "schema content";
        ContentAccessor contentAccessor = mock(ContentAccessor.class);
        when(schemaStore.getSchema(anyString())).thenReturn(contentAccessor);
        when(contentAccessor.getContentAsString()).thenReturn(content);

        // When
        ResponseEntity<String> response = schemasService.getSchema(id);

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isEqualTo(content);
        verify(schemaStore, times(1)).getSchema(id);
    }

    @Test
    void getSchemaShouldThrowNotFoundException() {
        // Given
        String id = "schemaId";
        when(schemaStore.getSchema(anyString())).thenReturn(null);

        // When & Then
        assertThatThrownBy(() -> schemasService.getSchema(id))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("There is no Schema with id " + id);
        verify(schemaStore, times(1)).getSchema(id);
    }

    @Test
    void getSchemasShouldReturn200WithBody() {
        // Given
        Map<SchemaStore.SchemaType, List<String>> schemaListMap = Map.of(
                SchemaStore.SchemaType.ONTOLOGY, Arrays.asList("ontology1", "ontology2"),
                SchemaStore.SchemaType.SHAPE, Arrays.asList("shape1", "shape2"),
                SchemaStore.SchemaType.VOCABULARY, Arrays.asList("vocab1", "vocab2")
        );
        when(schemaStore.getSchemaList()).thenReturn(schemaListMap);

        // When
        ResponseEntity<OntologySchema> response = schemasService.getSchemas();

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody().getOntologies()).containsExactly("ontology1", "ontology2");
        assertThat(response.getBody().getShapes()).containsExactly("shape1", "shape2");
        assertThat(response.getBody().getVocabularies()).containsExactly("vocab1", "vocab2");
        verify(schemaStore, times(1)).getSchemaList();
    }

    @Test
    void getLatestSchemaShouldReturn200AndResponseBody() {
        // Given
        String type = "ONTOLOGY";
        String content = "latest schema content";
        ContentAccessor contentAccessor = mock(ContentAccessor.class);
        when(schemaStore.getCompositeSchema(any(SchemaStore.SchemaType.class)))
                .thenReturn(contentAccessor);
        when(contentAccessor.getContentAsString()).thenReturn(content);

        // When
        ResponseEntity<String> response = schemasService.getLatestSchema(type, null);

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isEqualTo(content);
        verify(schemaStore, times(1)).getCompositeSchema(SchemaStore.SchemaType.ONTOLOGY);
    }

    @Test
    void getLatestSchemaShouldThrowException() {
        // Given
        String type = "INVALID_TYPE";

        // When & Then
        assertThatThrownBy(() -> schemasService.getLatestSchema(type, null))
                .isInstanceOf(ClientException.class)
                .hasMessageContaining("Please check the value of the type query parameter!");
        verify(schemaStore, never()).getCompositeSchema(any());
    }

    @Test
    void addSchemaShouldReturn201AndBody() {
        // Given
        String schema = "schema content";
        String schemaId = "newSchemaId";
        when(schemaStore.addSchema(any(ContentAccessor.class))).thenReturn(schemaId);

        // When
        ResponseEntity<Void> response = schemasService.addSchema(schema);

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(201);
        assertThat(response.getHeaders().getLocation()).isEqualTo(URI.create("/schemas/" + schemaId));
        verify(schemaStore, times(1)).addSchema(any(ContentAccessor.class));
    }

    @Test
    void deleteSchemaShouldReturn200() {
        // Given
        String id = "schemaId";

        // When
        ResponseEntity<Void> response = schemasService.deleteSchema(id);

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void updateSchemaShouldReturn200() {
        // Given
        String id = "schemaId";
        String schema = "updated schema content";

        // When
        ResponseEntity<Void> response = schemasService.updateSchema(id, schema);

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(schemaStore, times(1)).updateSchema(eq(id), any(ContentAccessor.class));
    }
}
package eu.xfsc.fc.server.service.merge;

import eu.xfsc.fc.api.generated.model.Results;
import eu.xfsc.fc.server.config.QueryProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FirstMergeStateTest {
  private FirstMergeState firstMergeState;
  private Results results;
  private Results local;
  private QueryProperties queryProps;

  @BeforeEach
  public void setUp() {
    firstMergeState = new FirstMergeState();
    results = new Results();
    local = new Results();
    queryProps = new QueryProperties();
  }

  @Test
  public void mergeShouldAddItemsToResults() {
    results.setItems(new ArrayList<>());
    Map<String, Object> item = new HashMap<>();
    item.put("items", List.of(Map.of("key1", "value1"), Map.of("key2", "value2")));

    firstMergeState.merge(results, item, local);

    assertEquals(2, results.getItems().size());
    assertTrue(results.getItems().containsAll((List<Map<String, Object>>) item.get("items")));
  }

  @Test
  public void mergeShouldHandleNullItems() {
    results.setItems(new ArrayList<>());
    Map<String, Object> item = new HashMap<>();
    item.put("items", null);

    firstMergeState.merge(results, item, local);

    assertEquals(0, results.getItems().size());
  }

  @Test
  public void mergeLocalShouldAddLocalItemsToResults() {
    results.setItems(new ArrayList<>());
    local.setItems(new ArrayList<>());
    local.getItems().add(Map.of("key1", "value1"));
    local.getItems().add(Map.of("key2", "value2"));

    firstMergeState.mergeLocal(results, local, queryProps);

    assertEquals(2, results.getItems().size());
    assertTrue(results.getItems().containsAll(local.getItems()));
  }
}
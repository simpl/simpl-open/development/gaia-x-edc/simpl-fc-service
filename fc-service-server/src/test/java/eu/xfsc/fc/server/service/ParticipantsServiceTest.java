package eu.xfsc.fc.server.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import eu.xfsc.fc.api.generated.model.Participant;
import eu.xfsc.fc.api.generated.model.Participants;
import eu.xfsc.fc.api.generated.model.UserProfile;
import eu.xfsc.fc.api.generated.model.UserProfiles;
import eu.xfsc.fc.core.dao.ParticipantDao;
import eu.xfsc.fc.core.dao.ValidatorCacheDao;
import eu.xfsc.fc.core.exception.NotFoundException;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.pojo.PaginatedResults;
import eu.xfsc.fc.core.pojo.ParticipantMetaData;
import eu.xfsc.fc.core.pojo.SdFilter;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.pojo.VerificationResultParticipant;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.VerificationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Optional;
import java.util.Arrays;
import java.util.Collections;

@ExtendWith(MockitoExtension.class)
class ParticipantsServiceTest {

    @Mock
    private ParticipantDao partDao;

    @Mock
    private ValidatorCacheDao validatorCache;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private SelfDescriptionStore sdStorePublisher;

    @Mock
    private VerificationService verificationService;

    @InjectMocks
    private ParticipantsService participantsService;

    @Test
    void addParticipantShouldCreateParticipant() {
        final var body = "test-body";
        final var verificationResult = mock(VerificationResultParticipant.class);
        final var participantMetaData = mock(ParticipantMetaData.class);

        when(verificationService.verifyParticipantSelfDescription(any())).thenReturn(verificationResult);
        when(partDao.create(any())).thenReturn(participantMetaData);
        when(participantMetaData.getId()).thenReturn("id");

        final var response = participantsService.addParticipant(body);

        verify(partDao).create(any());
        verify(sdStorePublisher).storeSelfDescription(any(), any());

        assertThat(response.getStatusCodeValue()).isEqualTo(201);
        assertThat(response.getHeaders().getLocation()).isEqualTo(URI.create("/participants/id"));
    }

    @Test
    void deleteParticipantShouldDeleteParticipant() {
        final var participantId = "participant-id";

        final var participantMetaData = mock(ParticipantMetaData.class);
        when(partDao.select(participantId)).thenReturn(Optional.of(participantMetaData));
        when(sdStorePublisher.getByHash(any()).getSelfDescription().getContentAsString()).thenReturn("string");
        when(partDao.delete(participantMetaData.getId())).thenReturn(Optional.of(participantMetaData));

        final var response = participantsService.deleteParticipant(participantId);

        verify(sdStorePublisher).deleteSelfDescription(any());
        verify(partDao).delete(any());

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void deleteParticipantShouldThrowNotFoundException() {
        String participantId = "participant-id";

        when(partDao.select(participantId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> participantsService.deleteParticipant(participantId))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("Participant not found: " + participantId);
    }

    @Test
    void getParticipantShouldReturnParticipant() {
        String participantId = "participant-id";

        final var participantMetaData = mock(ParticipantMetaData.class);
        final var selfDescriptionMetadata = mock(SelfDescriptionMetadata.class, RETURNS_DEEP_STUBS);
        when(selfDescriptionMetadata.getSelfDescription().getContentAsString()).thenReturn("lala");

        when(partDao.select(participantId)).thenReturn(Optional.of(participantMetaData));
        when(sdStorePublisher.getByHash(any())).thenReturn(selfDescriptionMetadata);

        final var response = participantsService.getParticipant(participantId);

        verify(partDao).select(participantId);
        verify(sdStorePublisher).getByHash(any());

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void getParticipantShouldThrowNotFoundException() {
        String participantId = "participant-id";

        when(partDao.select(participantId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> participantsService.getParticipant(participantId))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("Participant not found: " + participantId);
    }

    @Test
    void getParticipantUsersShouldReturnUserProfiles() {
        String participantId = "participant-id";

        final var paginatedResults = mock(PaginatedResults.class);
        when(partDao.selectUsers(participantId, 0, 10)).thenReturn(Optional.of(paginatedResults));
        when(paginatedResults.getTotalCount()).thenReturn(1L);
        when(paginatedResults.getResults()).thenReturn(Collections.singletonList(mock(UserProfile.class)));

        ResponseEntity<UserProfiles> response = participantsService.getParticipantUsers(participantId, 0, 10);

        verify(partDao).selectUsers(participantId, 0, 10);

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void getParticipantsShouldReturnParticipants() {
        PaginatedResults<ParticipantMetaData> paginatedResults = mock(PaginatedResults.class);
        when(partDao.search(0, 10)).thenReturn(paginatedResults);
        when(paginatedResults.getTotalCount()).thenReturn(2L);

        final var participantMetaData = new ParticipantMetaData("aaaa", "bbbb", "cccc", "ddd");
        when(paginatedResults.getResults()).thenReturn(Arrays.asList(participantMetaData));

        PaginatedResults<SelfDescriptionMetadata> sdPaginatedResults = mock(PaginatedResults.class);
        when(sdStorePublisher.getByFilter(any(), eq(true), eq(true))).thenReturn(sdPaginatedResults);
        when(sdPaginatedResults.getTotalCount()).thenReturn(2L);

        final var selfDescriptionMetaData = new SelfDescriptionMetadata();
        selfDescriptionMetaData.setSdHash("730f75dafd73e047b86acb2dbd74e75dcb93272fa084a9082848f2341aa1abb6");
        selfDescriptionMetaData.setSelfDescription(new ContentAccessorDirect("blabla"));
        when(sdPaginatedResults.getResults()).thenReturn(Arrays.asList(selfDescriptionMetaData));

        ResponseEntity<Participants> response = participantsService.getParticipants(0, 10);

        verify(partDao, times(1)).search(0, 10);
        verify(sdStorePublisher, times(1)).getByFilter(any(SdFilter.class), eq(true), eq(true));

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void updateParticipantShouldUpdateParticipant() {
        String participantId = "participant-id";
        String body = "test-body";
        VerificationResultParticipant verificationResult = mock(VerificationResultParticipant.class);
        SelfDescriptionMetadata selfDescriptionMetadata = mock(SelfDescriptionMetadata.class);
        ParticipantMetaData participantMetaData = mock(ParticipantMetaData.class);

        when(verificationResult.getId()).thenReturn("id");
        when(participantMetaData.getId()).thenReturn("id");
        when(partDao.select(participantId)).thenReturn(Optional.of(participantMetaData));
        when(verificationService.verifyParticipantSelfDescription(any())).thenReturn(verificationResult);
        when(partDao.update(any(), any())).thenReturn(Optional.of(participantMetaData));

        ResponseEntity<Participant> response = participantsService.updateParticipant(participantId, body);

        verify(sdStorePublisher, times(1)).storeSelfDescription(any(), any());

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void updateParticipantShouldThrowNotFoundException() {
        String participantId = "participant-id";
        String body = "test-body";
        when(partDao.select(participantId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> participantsService.updateParticipant(participantId, body))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("Participant not found: " + participantId);
    }
}

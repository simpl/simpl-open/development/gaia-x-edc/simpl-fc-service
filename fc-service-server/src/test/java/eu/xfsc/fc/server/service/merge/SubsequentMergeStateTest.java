package eu.xfsc.fc.server.service.merge;
import eu.xfsc.fc.api.generated.model.Results;
import eu.xfsc.fc.server.config.QueryProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubsequentMergeStateTest {
    private SubsequentMergeState subsequentMergeState;
    private Results results;
    private Results local;
    private QueryProperties queryProps;

    @BeforeEach
    public void setUp() {
        subsequentMergeState = new SubsequentMergeState();
        results = new Results();
        local = new Results();
        queryProps = new QueryProperties();
        queryProps.setSelf("server1");
    }

    @Test
    public void mergeShouldAddItemsToResults() {
        Map<String, Object> item = Map.of("key", "value");
        subsequentMergeState.merge(results, item, local);
        assertEquals(1, results.getItems().size());
        assertEquals(item, results.getItems().get(0));
    }

    @Test
    public void mergeLocalShouldAddLocalItemsToResults() {
        local.setTotalCount(5);
        local.setItems(List.of(Map.of("key", "value")));
        subsequentMergeState.mergeLocal(results, local, queryProps);
        assertEquals(1, results.getItems().size());
        Map<String, Object> expectedItem = Map.of(
            "server", "server1",
            "total", 5,
            "items", List.of(Map.of("key", "value"))
        );
        assertEquals(expectedItem, results.getItems().get(0));
    }
}

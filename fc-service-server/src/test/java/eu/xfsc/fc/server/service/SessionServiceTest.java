package eu.xfsc.fc.server.service;

import eu.xfsc.fc.core.dao.SessionDao;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
class SessionServiceTest {

    @Mock
    private SessionDao ssnDao;

    @InjectMocks
    private SessionService sessionService;

    @Test
    void getCurrentSessionShouldThrowException() {
        assertThatThrownBy(() -> sessionService.getCurrentSession())
                .isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void logoutCurrentSessionShouldThrowException() {
        assertThatThrownBy(() -> sessionService.logoutCurrentSession())
                .isInstanceOf(UnsupportedOperationException.class);
    }
}
package eu.xfsc.fc.server.service;


import java.net.URI;
import java.util.List;

import eu.xfsc.fc.api.generated.model.User;
import eu.xfsc.fc.api.generated.model.UserProfile;
import eu.xfsc.fc.core.dao.ParticipantDao;
import eu.xfsc.fc.core.dao.UserDao;
import eu.xfsc.fc.core.exception.ClientException;
import eu.xfsc.fc.core.exception.ConflictException;
import eu.xfsc.fc.core.pojo.PaginatedResults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static eu.xfsc.fc.server.util.CommonConstants.PARTICIPANT_ADMIN_ROLE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UsersServiceTest {

    @Mock
    private UserDao userDao;

    @Mock
    private ParticipantDao partDao;

    @InjectMocks
    private UsersService usersService;

    @Test
    void addUserShouldReturnCreatedWhenValidUser() {

        String participantId = "participantId";
        // Given
        User user = new User();
        user.setParticipantId(participantId);
        user.setEmail("email@example.com");
        user.setFirstName("John");
        user.setLastName("Doe");


        UserProfile userProfile = new UserProfile();
        userProfile.setId("1");

        when(userDao.create(user)).thenReturn(userProfile);

        // When
        ResponseEntity<UserProfile> response = usersService.addUser(user);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getHeaders().getLocation()).isEqualTo(URI.create("/users/1"));
        assertThat(response.getBody()).isEqualTo(userProfile);
    }

    @Test
    void addUserShouldThrowClientExceptionWhenUserIsInvalid() {
        // Given
        User user = new User(); // Empty user

        // When / Then
        assertThatThrownBy(() -> usersService.addUser(user))
                .isInstanceOf(ClientException.class)
                .hasMessage("User cannot be empty or have empty field values, except for the role!");
    }

    @Test
    void updateUserShouldReturnOkWhenValidUser() {
        // Given
        String userId = "userId";
        User user = new User();
        user.setParticipantId("participantId");
        user.setEmail("email@example.com");
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setRoleIds(List.of("Ro-MU-A"));

        UserProfile userProfile = new UserProfile();
        userProfile.setParticipantId("participantId");

        when(userDao.update(userId, user)).thenReturn(userProfile);

        // When
        ResponseEntity<UserProfile> response = usersService.updateUser(userId, user);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(userProfile);

        verify(userDao).update(userId, user);
    }

    @Test
    void updateUserShouldThrowClientExceptionWhenUserIsInvalid() {
        // Given
        String userId = "userId";
        User user = new User(); // Empty user

        // When / Then
        assertThatThrownBy(() -> usersService.updateUser(userId, user))
                .isInstanceOf(ClientException.class)
                .hasMessage("User cannot be empty or have empty field values, except for the role!");
    }

    @Test
    void deleteUserShouldReturnOkWhenValidUserId() {
        // Given
        String userId = "userId";
        UserProfile userProfile = new UserProfile();
        userProfile.setParticipantId("participantId");
        userProfile.setRoleIds(List.of("Ro-MU-A"));

        UserProfile userProfile2 = new UserProfile();
        userProfile2.setParticipantId("participantId");
        userProfile2.setRoleIds(List.of("Ro-MU-A"));

        PaginatedResults<UserProfile> profiles = new PaginatedResults<>(List.of(userProfile, userProfile2));

        when(userDao.select(userId)).thenReturn(userProfile);
        when(userDao.search(any(), any(), any())).thenReturn(profiles);
        when(userDao.delete(userId)).thenReturn(userProfile);

        // When
        ResponseEntity<UserProfile> response = usersService.deleteUser(userId);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(userProfile);

    }

    @Test
    void deleteUserShouldThrowConflictExceptionWhenDeletingLastParticipantAdmin() {
        // Given
        String userId = "userId";
        UserProfile userProfile = new UserProfile();
        userProfile.setParticipantId("participantId");
        userProfile.setRoleIds(List.of(PARTICIPANT_ADMIN_ROLE));


        PaginatedResults<UserProfile> profiles = new PaginatedResults<>(List.of(userProfile));

        when(userDao.select(userId)).thenReturn(userProfile);
        when(userDao.search(any(), any(), any())).thenReturn(profiles);

        // When / Then
        assertThatThrownBy(() -> usersService.deleteUser(userId))
                .isInstanceOf(ConflictException.class)
                .hasMessage("Last participant admin cannot be deleted");

        verify(userDao, never()).delete(any());
    }

    @Test
    void getUserShouldReturnUserProfileWhenValidUserId() {
        // Given
        String userId = "userId";
        UserProfile userProfile = new UserProfile();
        userProfile.setParticipantId(userId);

        when(userDao.select(userId)).thenReturn(userProfile);

        // When
        ResponseEntity<UserProfile> response = usersService.getUser(userId);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(userProfile);
    }

    @Test
    void getUsersShouldThrowException() {
        assertThatThrownBy(() -> usersService.getUsers(1, 3))
                .isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void getUserRolesShouldReturnRolesWhenValidUserId() {
        // Given
        String userId = "userId";
        UserProfile userProfile = new UserProfile();
        userProfile.setRoleIds(List.of("role1", "role2"));

        when(userDao.select(userId)).thenReturn(userProfile);

        // When
        ResponseEntity<List<String>> response = usersService.getUserRoles(userId);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).containsExactly("role1", "role2");

        verify(userDao).select(userId);
    }

    @Test
    void updateUserRolesShouldReturnUpdatedUserProfileWhenValidRoles() {
        // Given
        String userId = "userId";
        List<String> roles = List.of("role1", "role2");
        UserProfile userProfile = new UserProfile();
        userProfile.setRoleIds(roles);

        when(userDao.updateRoles(userId, roles)).thenReturn(userProfile);

        // When
        ResponseEntity<UserProfile> response = usersService.updateUserRoles(userId, roles);

        // Then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).isEqualTo(userProfile);

        verify(userDao).updateRoles(userId, roles);
    }
}
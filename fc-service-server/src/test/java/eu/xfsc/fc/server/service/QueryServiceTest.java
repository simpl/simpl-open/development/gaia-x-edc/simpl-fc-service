package eu.xfsc.fc.server.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.xfsc.fc.api.generated.model.AnnotatedStatement;
import eu.xfsc.fc.api.generated.model.QueryLanguage;
import eu.xfsc.fc.api.generated.model.Results;
import eu.xfsc.fc.api.generated.model.Statement;
import eu.xfsc.fc.client.QueryClient;
import eu.xfsc.fc.core.exception.ServerException;
import eu.xfsc.fc.core.pojo.GraphQuery;
import eu.xfsc.fc.core.pojo.PaginatedResults;
import eu.xfsc.fc.core.service.graphdb.GraphStore;
import eu.xfsc.fc.server.config.QueryProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import reactor.core.publisher.Mono;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;


@ExtendWith(MockitoExtension.class)
class QueryServiceTest {

    @Mock
    private GraphStore graphStore;

    @Mock
    private ObjectMapper jsonMapper;

    @Mock
    private ResourceLoader resourceLoader;

    @Mock
    private QueryProperties queryProps;

    @InjectMocks
    private QueryService queryService;

    @Test
    void initClientsShouldNotThrowException() {
        List<String> partners = Arrays.asList("http://partner1", "http://partner2");
        when(queryProps.getPartners())
                .thenReturn(partners);

        queryService.initClients();
    }

    @Test
    void queryShouldNotThrowException() {
        // Given
        Statement statement = new Statement();
        statement.setStatement("MATCH (n) RETURN n");

        PaginatedResults<Map<String, Object>> paginatedResults = mock(PaginatedResults.class);
        when(graphStore.queryData(any())).thenReturn(paginatedResults);

        // When
        ResponseEntity<Results> response = queryService.query(
                QueryLanguage.OPENCYPHER, 30, true, statement);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void queryWebsiteShouldThrowException() throws IOException {
        // Given
        Resource resource = mock(Resource.class);
        when(resourceLoader.getResource("classpath:static/query.html")).thenReturn(resource);
        when(resource.getInputStream())
                .thenThrow(new IOException());

         assertThatThrownBy(() -> queryService.querywebsite())
                 .isInstanceOf(ServerException.class);
    }

    @Test
    void SearchWithDefaultLimitShouldNotThrowException() {
        // Given
        AnnotatedStatement statement = new AnnotatedStatement();
        statement.setStatement("MATCH (n) RETURN n");

        PaginatedResults<Map<String, Object>> paginatedResults = mock(PaginatedResults.class);

        when(graphStore.queryData(any(GraphQuery.class))).thenReturn(paginatedResults);

        // When
        ResponseEntity<Results> response = queryService.search(statement);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void searchWithExtraResultsShouldMergeCorrectly() {
        // Given
        AnnotatedStatement statement = new AnnotatedStatement();
        statement.setStatement("MATCH (n) RETURN n");

        // Mock local graph store query results
        PaginatedResults<Map<String, Object>> paginatedResults = mock(PaginatedResults.class);
        when(graphStore.queryData(any(GraphQuery.class)))
            .thenReturn(paginatedResults);
        when(paginatedResults.getTotalCount()).thenReturn(10L); // Mock total count for local results
        when(paginatedResults.getResults())
            .thenReturn(List.of(Map.of("key", "value"))); // Mock local results

        // Mock extra results from partners
        Results extraResult1 = new Results(
            5,
            List.of(Map.of(
                "server", "server1",
                "items", List.of(Map.of("key", "value1")),
                "total", 5
            ))
        );
        Results extraResult2 = new Results(
            3,
            List.of(Map.of(
                "server", "server2",
                "items", List.of(Map.of("key", "value2")),
                "total", 3
            ))
        );

        // Mock QueryClient behavior
        QueryClient mockClient1 = mock(QueryClient.class);
        QueryClient mockClient2 = mock(QueryClient.class);
        when(mockClient1.searchAsync(any(AnnotatedStatement.class)))
            .thenReturn(Mono.just(extraResult1)); // Partner 1 response
        when(mockClient2.searchAsync(any(AnnotatedStatement.class)))
            .thenReturn(Mono.just(extraResult2)); // Partner 2 response

        // Mock QueryProperties
        when(queryProps.getSelf()).thenReturn("self-url");
        when(queryProps.getPartners()).thenReturn(List.of("partner-url1", "partner-url2"));

        queryService.setQueryClients(List.of(mockClient1, mockClient2)); // Inject the mocked QueryClients

        // When
        ResponseEntity<Results> response = queryService.search(statement);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();

        // Verify the merged results
        assertThat(response.getBody().getTotalCount()).isEqualTo(18); // 10 local + 5 + 3
        assertThat(response.getBody().getItems())
            .containsExactlyInAnyOrder(
                Map.of("key", "value"),  // Local result
                Map.of("key", "value1"), // Partner 1 result
                Map.of("key", "value2")  // Partner 2 result
            );
    }





    @Test
     void searchWithPartnerResultsShouldNotThrowException() {
        // Given
        AnnotatedStatement statement = new AnnotatedStatement();
        statement.setStatement("MATCH (n) RETURN n");

        PaginatedResults<Map<String, Object>> paginatedResults = mock(PaginatedResults.class);
        when(graphStore.queryData(any(GraphQuery.class))).thenReturn(paginatedResults);

        when(queryProps.getSelf()).thenReturn("self-url");
        when(queryProps.getPartners()).thenReturn(Collections.singletonList("partner-url"));


        // When
        QueryClient client = new QueryClient("baseUrl", "jwt");
        queryService.setQueryClients(List.of(client));
        ResponseEntity<Results> response = queryService.search(statement);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void testCheckIfLimitAbsent() {
        // Given
        String queryWithLimit = "MATCH (n) RETURN n LIMIT 10";
        String queryWithoutLimit = "MATCH (n) RETURN n";

        Statement statementWithoutLimit = new Statement();
        statementWithoutLimit.setStatement(queryWithoutLimit);

        Statement statementWithLimit = new Statement();
        statementWithLimit.setStatement(queryWithLimit);



        PaginatedResults<Map<String, Object>> paginatedResults = mock(PaginatedResults.class);
        when(graphStore.queryData(any())).thenReturn(paginatedResults);

        queryService.query(QueryLanguage.OPENCYPHER, 30, true, statementWithoutLimit);
        assertThat(statementWithoutLimit.getStatement()).isEqualTo("MATCH (n) RETURN n limit $limit");

        queryService.query(QueryLanguage.OPENCYPHER, 30, true, statementWithLimit);
        assertThat(statementWithLimit.getStatement()).isEqualTo(queryWithLimit);
    }
}
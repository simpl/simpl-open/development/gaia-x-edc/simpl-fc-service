package eu.xfsc.fc.core.service.pubsub.event;

import java.util.Map;


import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;

public class DeleteEventHandler implements EventHandler {
    private final SelfDescriptionStore sdStore;

    public DeleteEventHandler(SelfDescriptionStore sdStore) {
        this.sdStore = sdStore;
    }

    @Override
    public void handle(Map<String, Object> params) {
        String hash = (String) params.get("hash");
        sdStore.deleteSelfDescription(hash);
    }
}

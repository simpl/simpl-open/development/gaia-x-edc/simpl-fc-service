package eu.xfsc.fc.core.service.pubsub;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.xfsc.fc.client.SelfDescriptionClient;
import eu.xfsc.fc.core.service.pubsub.SDPublisher.SDEvent;
import eu.xfsc.fc.core.service.pubsub.event.EventHandler;
import eu.xfsc.fc.core.service.pubsub.event.EventHandlerFactory;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.VerificationService;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseSDSubscriber implements SDSubscriber {

	protected static final TypeReference<HashMap<String, Object>> mapTypeRef = new TypeReference<HashMap<String, Object>>() {};
		
    @Value("${subscriber.instance}")
    protected String instance;
    @Autowired 
	protected SelfDescriptionStore sdStore;
    @Autowired 
	protected VerificationService verificationService;
    @Autowired 
	protected ObjectMapper jsonMapper;

	protected Map<String, SelfDescriptionClient> sdClients = new HashMap<>();
    
    @PostConstruct
    public void init() throws Exception {
    	subscribe();
    }
	
	@Override
	public void onMessage(Map<String, Object> params) {
		log.debug("onMessage.enter; got params: {}", params);
		
		SDEvent event = SDEvent.valueOf((String) params.get("event"));
	
		// Create the factory (you could inject this in a real application)
		EventHandlerFactory factory = new EventHandlerFactory(sdStore, verificationService, sdClients, jsonMapper);
		
		// Retrieve the appropriate handler and handle the event
		EventHandler handler = factory.getHandler(event);
		handler.handle(params);
	}
	
    protected abstract void subscribe() throws Exception;
	
}

package eu.xfsc.fc.core.service.schemastore.schematypestrategy;

import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import static eu.xfsc.fc.core.util.SchemaStoreUtils.addUrlsToSetForGivenNodeFromModel;

public class OntologyUrlsExtractor implements UrlsExtractor {
    @Override
    public void addUrlsFromModelToSet(Model model, Set<String> extractedUrlsSet) {
        addUrlsToSetForGivenNodeFromModel(model, OWL2.NamedIndividual, extractedUrlsSet);
        addUrlsToSetForGivenNodeFromModel(model, RDF.Property, extractedUrlsSet);
        addUrlsToSetForGivenNodeFromModel(model, OWL2.DatatypeProperty, extractedUrlsSet);
        addUrlsToSetForGivenNodeFromModel(model, OWL2.ObjectProperty, extractedUrlsSet);
        addUrlsToSetForGivenNodeFromModel(model, RDFS.Class, extractedUrlsSet);
        addUrlsToSetForGivenNodeFromModel(model, OWL2.Class, extractedUrlsSet);
    }
}

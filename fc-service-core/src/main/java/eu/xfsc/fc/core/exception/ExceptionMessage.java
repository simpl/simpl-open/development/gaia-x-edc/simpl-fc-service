package eu.xfsc.fc.core.exception;


public class ExceptionMessage {

    private ExceptionMessage() {}

    public static final String CONFLICT_EXCEPTION_MESSAGE = "A file for the hash %s already exists.";
    public static final String FILE_NOT_FOUND_EXCEPTION_MESSAGE = "A file for the hash %s does not exist.";

}
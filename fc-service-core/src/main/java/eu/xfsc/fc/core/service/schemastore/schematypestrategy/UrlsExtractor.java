package eu.xfsc.fc.core.service.schemastore.schematypestrategy;

import java.util.Set;

import org.apache.jena.rdf.model.Model;

public interface UrlsExtractor {
    void addUrlsFromModelToSet(Model model, Set<String> extractedUrlsSet);
}

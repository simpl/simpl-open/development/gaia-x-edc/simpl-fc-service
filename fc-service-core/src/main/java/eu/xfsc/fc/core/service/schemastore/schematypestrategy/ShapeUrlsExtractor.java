package eu.xfsc.fc.core.service.schemastore.schematypestrategy;

import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.shacl.vocabulary.SHACLM;

import static eu.xfsc.fc.core.util.SchemaStoreUtils.addUrlsToSetForGivenNodeFromModel;

public class ShapeUrlsExtractor implements UrlsExtractor {
    @Override
    public void addUrlsFromModelToSet(Model model, Set<String> extractedUrlsSet) {
        addUrlsToSetForGivenNodeFromModel(model, SHACLM.NodeShape, extractedUrlsSet);
        addUrlsToSetForGivenNodeFromModel(model, SHACLM.PropertyShape, extractedUrlsSet);
    }
}

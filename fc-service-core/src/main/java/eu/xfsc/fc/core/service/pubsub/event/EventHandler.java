package eu.xfsc.fc.core.service.pubsub.event;

import java.util.Map;

public interface EventHandler {
    void handle(Map<String, Object> params);
}

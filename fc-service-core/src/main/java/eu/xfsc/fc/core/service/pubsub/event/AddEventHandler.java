package eu.xfsc.fc.core.service.pubsub.event;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.xfsc.fc.api.generated.model.SelfDescriptionResult;
import eu.xfsc.fc.client.SelfDescriptionClient;
import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.pojo.VerificationResult;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.VerificationService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AddEventHandler implements EventHandler {
    private final SelfDescriptionStore sdStore;
    private final VerificationService verificationService;
    private final Map<String, SelfDescriptionClient> sdClients;
    private final ObjectMapper jsonMapper;

    public AddEventHandler(SelfDescriptionStore sdStore, VerificationService verificationService, Map<String, SelfDescriptionClient> sdClients, ObjectMapper jsonMapper) {
        this.sdStore = sdStore;
        this.verificationService = verificationService;
        this.sdClients = sdClients;
        this.jsonMapper = jsonMapper;
    }

    @Override
    public void handle(Map<String, Object> params) {
        try {
            VerificationResult vr;
            SelfDescriptionMetadata sdMeta;
            String dataStr = (String) params.get("data");
            String hash = (String) params.get("hash");
            
            if (dataStr == null) {
                String source = (String) params.get("source");
                SelfDescriptionClient sdClient = sdClients.computeIfAbsent(source, src -> new SelfDescriptionClient(src, (String) null));
                SelfDescriptionResult sd = sdClient.getSelfDescriptionByHash(hash, false, true);
                ContentAccessor content = new ContentAccessorDirect(sd.getContent());
                vr = verificationService.verifyOfferingSelfDescription(content);
                sdMeta = new SelfDescriptionMetadata(vr.getId(), vr.getIssuer(), vr.getValidators(), content);
            } else {
                Map<String, Object> data = jsonMapper.readValue(dataStr, new TypeReference<Map<String, Object>>() {});
                String vrs = (String) data.get("verificationResult");
                vr = jsonMapper.readValue(vrs, VerificationResult.class);
                String content = (String) data.get("content");
                sdMeta = new SelfDescriptionMetadata(new ContentAccessorDirect(content), vr);
            }
            sdStore.storeSelfDescription(sdMeta, vr);
        } catch (JsonProcessingException ex) {
            log.warn("onMessage.error", ex);
        }
    }
}

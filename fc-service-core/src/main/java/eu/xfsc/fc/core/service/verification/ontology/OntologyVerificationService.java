package eu.xfsc.fc.core.service.verification.ontology;

import java.io.StringReader;
import java.util.Iterator;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.xfsc.fc.core.exception.VerificationException;
import eu.xfsc.fc.core.service.schemastore.SchemaStore;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OntologyVerificationService {

    private final SchemaStore schemaStore;

    @SneakyThrows
    public void verifyPayloadAgainstOntology(final String payload) {
        log.debug("verifyPayloadAgainstOntology.enter;");
        final var ontologyShapeAsString = schemaStore.getCompositeSchema(SchemaStore.SchemaType.ONTOLOGY).getContentAsString();

        // Load the ontology model
        final var ontologyModel = ModelFactory.createDefaultModel();
        ontologyModel.read(new StringReader(ontologyShapeAsString), null, "TURTLE");

        final var objectMapper = new ObjectMapper();
        final var jsonNode = objectMapper.readTree(payload);
        final var credentialSubjectAsString = objectMapper.writeValueAsString(jsonNode.get("credentialSubject"));

        // Load the data model from JSON-LD
        final var dataModel = ModelFactory.createDefaultModel();
        RDFParser.create().fromString(credentialSubjectAsString)
                .lang(RDFLanguages.JSONLD)
                .parse(dataModel);

        final var reasoner = ReasonerRegistry.getOWLReasoner();

        // Create an inference model using the reasoner and the data model
        final var infModel = ModelFactory.createInfModel(reasoner, dataModel);

        // Add the ontology model to the inference model
        infModel.add(ontologyModel);

        // Validate the model for consistency
        final var validity = infModel.validate();


        if(!validity.isValid()) {
            final var errorReport = new StringBuilder();
            errorReport.append("Inference model validation error.\n");
            for (Iterator i = validity.getReports(); i.hasNext(); ) {
                errorReport.append(i.next());
                errorReport.append("\n");
            }
            throw new VerificationException(errorReport.toString());
        }

        validateDataAgainstOntologyDomain(ontologyModel, infModel);
    }

    private void validateDataAgainstOntologyDomain(final Model ontologyModel, final InfModel infModel) {
        // Iterate over each statement in the data model
        StmtIterator stmtIterator = infModel.listStatements();
        final var errorReport = new StringBuilder();

        while (stmtIterator.hasNext()) {
            final var semanticTriple = stmtIterator.nextStatement();

            final var dataSubject = semanticTriple.getSubject();
            final var dataPredicate = semanticTriple.getPredicate();
            final var dataObject = semanticTriple.getObject();

            // Check if property has domain
            Resource expectedOntologyDomain = getResource(ontologyModel, dataPredicate, RDFS.domain);

            if (expectedOntologyDomain != null) {

                final var actualTypeAsString = infModel.getProperty(dataSubject, RDF.type).getObject().toString();

                // Check if the subject is of the correct type (domain)
                if (!hasTypeOrSubclass(infModel, dataSubject, expectedOntologyDomain)) {

                    errorReport.append("Domain validation error: dataObject: ")
                            .append(dataObject)
                            .append(" for predicate: ")
                            .append(dataPredicate)
                            .append(" has type: ")
                            .append(actualTypeAsString)
                            .append(". Expected domain: ")
                            .append(expectedOntologyDomain)
                            .append("\n");
                }
            }
        }

        if(!errorReport.toString().isEmpty()) {
            throw new VerificationException(errorReport.toString());
        }
    }

    private Resource getResource(Model model, Property property, Property p) {
        StmtIterator iterator = model.listStatements(property, p, (RDFNode) null);
        if (iterator.hasNext()) {
            return iterator.nextStatement().getObject().asResource();
        }
        return null;
    }

    private static boolean hasTypeOrSubclass(InfModel infModel, Resource dataSubject, Resource expectedOntologyDomain) {
        final var actualTypeOfDataSubject =  infModel.getProperty(dataSubject, RDF.type);

        return actualTypeOfDataSubject.equals(expectedOntologyDomain)
                || infModel.contains(actualTypeOfDataSubject.getResource(), RDFS.subClassOf, expectedOntologyDomain);
    }
}

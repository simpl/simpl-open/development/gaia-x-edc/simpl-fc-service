package eu.xfsc.fc.core.util;

import java.util.Set;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SchemaStoreUtils {

    public static void addUrlsToSetForGivenNodeFromModel(Model model, RDFNode node, Set<String> extractedSet) {
        ResIterator resIteratorNode = model.listResourcesWithProperty(RDF.type, node);
        while (resIteratorNode.hasNext()) {
            Resource rs = resIteratorNode.nextResource();
            extractedSet.add(rs.getURI());
        }
    }
}

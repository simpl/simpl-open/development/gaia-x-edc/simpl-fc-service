package eu.xfsc.fc.core.service.pubsub.event;

import java.util.Map;

import eu.xfsc.fc.api.generated.model.SelfDescriptionStatus;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;

public class UpdateEventHandler implements EventHandler {
    private final SelfDescriptionStore sdStore;

    public UpdateEventHandler(SelfDescriptionStore sdStore) {
        this.sdStore = sdStore;
    }

    @Override
    public void handle(Map<String, Object> params) {
        String hash = (String) params.get("hash");
        String statusStr = (String) params.get("status");
        SelfDescriptionStatus status = SelfDescriptionStatus.valueOf(statusStr);
        sdStore.changeLifeCycleStatus(hash, status);
    }
}
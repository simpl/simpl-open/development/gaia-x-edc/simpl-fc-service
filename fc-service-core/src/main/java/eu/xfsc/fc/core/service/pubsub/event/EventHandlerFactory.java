package eu.xfsc.fc.core.service.pubsub.event;

import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

import eu.xfsc.fc.client.SelfDescriptionClient;
import eu.xfsc.fc.core.service.pubsub.SDPublisher.SDEvent;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.VerificationService;


public class EventHandlerFactory {
    private final Map<SDEvent, EventHandler> handlers = new HashMap<>();

    public EventHandlerFactory(SelfDescriptionStore sdStore, VerificationService verificationService, Map<String, SelfDescriptionClient> sdClients, ObjectMapper jsonMapper) {
        // Register each event handler
        handlers.put(SDEvent.ADD, new AddEventHandler(sdStore, verificationService, sdClients, jsonMapper));
        handlers.put(SDEvent.UPDATE, new UpdateEventHandler(sdStore));
        handlers.put(SDEvent.DELETE, new DeleteEventHandler(sdStore));
    }

    public EventHandler getHandler(SDEvent event) {
        EventHandler handler = handlers.get(event);
        if (handler == null) {
            throw new IllegalArgumentException("No handler registered for event: " + event);
        }
        return handler;
    }
}
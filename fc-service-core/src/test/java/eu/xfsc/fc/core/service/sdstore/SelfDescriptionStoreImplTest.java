package eu.xfsc.fc.core.service.sdstore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.xfsc.fc.api.generated.model.SelfDescriptionStatus;
import eu.xfsc.fc.core.dao.SelfDescriptionDao;
import eu.xfsc.fc.core.exception.ConflictException;
import eu.xfsc.fc.core.exception.NotFoundException;
import eu.xfsc.fc.core.exception.ServerException;
import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.pojo.PaginatedResults;
import eu.xfsc.fc.core.pojo.SdFilter;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.pojo.Validator;
import eu.xfsc.fc.core.pojo.VerificationResult;
import eu.xfsc.fc.core.service.graphdb.GraphStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DuplicateKeyException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SelfDescriptionStoreImplTest {

    @Mock
    private SelfDescriptionDao dao;

    @Mock
    private GraphStore graphDb;

    @InjectMocks
    private SelfDescriptionStoreImpl selfDescriptionStoreImpl;

    @Test
    void getSDFileByHashShouldReturnContentAccessor() {
        final var sdmRecord = mock(SdMetaRecord.class);
        when(dao.select(anyString())).thenReturn(sdmRecord);
        when(sdmRecord.getSelfDescription()).thenReturn(new ContentAccessorDirect("lala"));

        ContentAccessor result = selfDescriptionStoreImpl.getSDFileByHash("some-hash");

        assertThat(result).isNotNull();
    }

    @Test
    void getSDFileByHashShouldThrowNotFoundException() {
        when(dao.select(anyString())).thenReturn(null);

        assertThatThrownBy(() -> selfDescriptionStoreImpl.getSDFileByHash("some-hash"))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("no self-description found for hash");
    }

    @Test
    void getByIdShouldThrowExceptionWhenNoRecordFound() {
        when(dao.selectById(anyString()))
                .thenReturn(null);

        assertThatThrownBy(() -> selfDescriptionStoreImpl.getById("id"))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("no self-description found for id");
    }

    @Test
    void getByIdShouldReturnRecordWhenRecordIsFound() {
        final var sdmRecordMock = mock(SdMetaRecord.class);
        when(dao.selectById(anyString()))
                .thenReturn(sdmRecordMock);

        final var result = selfDescriptionStoreImpl.getById("id");

        assertThat(result)
                .isNotNull();
    }

    @Test
    void getByFilterShouldReturnPaginatedResults() {
        final var sdmRecord = mock(SdMetaRecord.class);
        PaginatedResults<SdMetaRecord> paginatedResults = new PaginatedResults<>(1L, Collections.singletonList(sdmRecord));
        when(dao.selectByFilter(any(), anyBoolean(), anyBoolean())).thenReturn(paginatedResults);

        PaginatedResults<SelfDescriptionMetadata> result = selfDescriptionStoreImpl.getByFilter(new SdFilter(), true, true);

        assertThat(result.getTotalCount()).isEqualTo(1L);
    }

    @Test
    void storeSelfDescriptionShouldThrowIllegalArgumentException() {
        final var sdMetadata = new SelfDescriptionMetadata();

        assertThatThrownBy(() -> selfDescriptionStoreImpl.storeSelfDescription(sdMetadata, null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("verification result must not be null");
    }

    @Test
    void storeSelfDescriptionShouldThrowConflictExceptionSdAlreadyExistsWithGivenHash() {
        final var sdMetadata = new SelfDescriptionMetadata();
        final var verificationResult = mock(VerificationResult.class);

        when(verificationResult.getValidators())
                .thenReturn(List.of(new Validator()));
        doThrow(new DuplicateKeyException("sdfiles_pkey"))
                .when(dao).insert(any());

        assertThatThrownBy(() -> selfDescriptionStoreImpl.storeSelfDescription(sdMetadata, verificationResult))
                .isInstanceOf(ConflictException.class)
                .hasMessageContaining("elf-description with hash");
    }

    @Test
    void storeSelfDescriptionShouldThrowConflictExceptionSdAlreadyExistsWithGivenSubjectId() {
        final var sdMetadata = new SelfDescriptionMetadata();
        final var verificationResult = mock(VerificationResult.class);

        when(verificationResult.getValidators())
                .thenReturn(List.of(new Validator()));
        doThrow(new DuplicateKeyException("idx_sd_file_is_active"))
                .when(dao).insert(any());

        assertThatThrownBy(() -> selfDescriptionStoreImpl.storeSelfDescription(sdMetadata, verificationResult))
                .isInstanceOf(ConflictException.class)
                .hasMessageContaining("active self-description with subjectId");
    }

    @Test
    void storeSelfDescriptionShouldThrowServerException() {
        final var sdMetadata = new SelfDescriptionMetadata();
        final var verificationResult = mock(VerificationResult.class);

        when(verificationResult.getValidators())
                .thenReturn(List.of(new Validator()));
        doThrow(new DuplicateKeyException("Some unexpected reason"))
                .when(dao).insert(any());

        assertThatThrownBy(() -> selfDescriptionStoreImpl.storeSelfDescription(sdMetadata, verificationResult))
                .isInstanceOf(ServerException.class)
                .hasMessageContaining("Some unexpected reason");
    }

    @Test
    void storeSelfDescriptionShouldNotThrowException() {
        final var sdMetadata = new SelfDescriptionMetadata();
        final var verificationResult = mock(VerificationResult.class);

        when(verificationResult.getValidators())
                .thenReturn(List.of(new Validator()));

        final var subjectHash = new SubjectHashRecord("subjectId", "hash");
        when(dao.insert(any()))
                .thenReturn(subjectHash);

        assertThatCode(() -> selfDescriptionStoreImpl.storeSelfDescription(sdMetadata, verificationResult))
                .doesNotThrowAnyException();
    }

    @Test
    void changeLifeCycleStatusShouldThrowNotFoundException() {
        final var hash = "hash";
        final var sdStatus = SelfDescriptionStatus.ACTIVE;

        assertThatThrownBy(() -> selfDescriptionStoreImpl.changeLifeCycleStatus(hash, sdStatus))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("no self-description found for hash");
    }

    @Test
    void changeLifeCycleStatusShouldThrowConflictException() {
        final var hash = "hash";
        final var sdStatus = SelfDescriptionStatus.ACTIVE;
        final var subjectStatusRecord = new SubjectStatusRecord(null, 3);

        when(dao.update(any(), anyInt()))
                .thenReturn(subjectStatusRecord);

        assertThatThrownBy(() -> selfDescriptionStoreImpl.changeLifeCycleStatus(hash, sdStatus))
                .isInstanceOf(ConflictException.class)
                .hasMessageContaining("can not change status of self-description");
    }

    @Test
    void changeLifeCycleStatusShouldNotThrowException() {
        final var hash = "hash";
        final var sdStatus = SelfDescriptionStatus.ACTIVE;
        final var subjectStatusRecord = new SubjectStatusRecord("subjectId", 3);

        when(dao.update(any(), anyInt()))
                .thenReturn(subjectStatusRecord);

        assertThatCode(() -> selfDescriptionStoreImpl.changeLifeCycleStatus(hash, sdStatus))
                .doesNotThrowAnyException();

        verify(graphDb).deleteClaims(anyString());
    }

        @Test
    void deleteSelfDescriptionShouldThrowNotFoundExceptionWhenNoRecordFound() {
        assertThatThrownBy(() -> selfDescriptionStoreImpl.deleteSelfDescription("some-hash"))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("no self-description found for hash some-hash");
    }

    @Test
    void deleteSelfDescriptionShouldDeleteAndRemoveClaims() {
        final var subjectStatusRecord = new SubjectStatusRecord("subjectId", 0);
        when(dao.delete(anyString())).thenReturn(subjectStatusRecord);

        selfDescriptionStoreImpl.deleteSelfDescription("some-hash");

        verify(graphDb).deleteClaims("subjectId");
        verify(dao).delete("some-hash");
    }

    @Test
    void invalidateSelfDescriptionsShouldNotThrowException() {
        when(dao.selectExpiredHashes())
                .thenReturn(List.of("hash"));

        final var subjectStatusRecord = new SubjectStatusRecord("subjectId", 0);
        when(dao.update(anyString(), anyInt()))
                .thenReturn(subjectStatusRecord);


       final var result = selfDescriptionStoreImpl.invalidateSelfDescriptions();

       assertThat(result).isEqualTo(1);
    }

    @Test
    void getActiveSdHashesShouldReturnActiveHashes() {
        when(dao.selectHashes(anyString(), anyInt(), anyInt(), anyInt())).thenReturn(Arrays.asList("hash1", "hash2"));

        List<String> result = selfDescriptionStoreImpl.getActiveSdHashes("after-hash", 10, 2, 1);

        assertThat(result).containsExactly("hash1", "hash2");
    }

    @Test
    void clearShouldCallDeleteAll() {
        selfDescriptionStoreImpl.clear();

        verify(dao).deleteAll();
    }
}
package eu.xfsc.fc.core.service.filestore;

import com.github.benmanes.caffeine.cache.Cache;
import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import org.apache.commons.io.FileExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

import static eu.xfsc.fc.core.exception.ExceptionMessage.CONFLICT_EXCEPTION_MESSAGE;
import static eu.xfsc.fc.core.exception.ExceptionMessage.FILE_NOT_FOUND_EXCEPTION_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CacheFileStoreTest {

    @Mock
    private Cache<String, String> dataCache;

    private CacheFileStore cacheFileStore;

    @BeforeEach
    void setUp() {
        cacheFileStore = new CacheFileStore(10);
        ReflectionTestUtils.setField(cacheFileStore, "dataCache", dataCache);
    }

    @Test
    void storeFileTest() throws IOException {
        var hash = "hash";
        var contentString = "content";
        var contentMock = mock(ContentAccessor.class);
        var mapSpy = spy(new ConcurrentHashMap<String, String>());

        when(contentMock.getContentAsString()).thenReturn(contentString);
        when(dataCache.asMap()).thenReturn(mapSpy);

        cacheFileStore.storeFile(hash, contentMock);

        verify(mapSpy).merge(eq(hash), eq(contentString), any(BiFunction.class));

    }

    @Test
    void storeFileThrowsExceptionTest() {
        var hash = "hash";
        var contentString = "content";
        var contentMock = mock(ContentAccessor.class);
        var mapSpy = spy(new ConcurrentHashMap<String, String>());
        mapSpy.put(hash, "existingContent");

        when(contentMock.getContentAsString()).thenReturn(contentString);
        when(dataCache.asMap()).thenReturn(mapSpy);

        var exception = assertThrows(FileExistsException.class, () -> cacheFileStore.storeFile(hash, contentMock));
        assertEquals(String.format(CONFLICT_EXCEPTION_MESSAGE, hash), exception.getMessage());
    }

    @Test
    void readFileThrowsExceptionTest() {
        var hash = "hash";

        when(dataCache.getIfPresent(hash)).thenReturn(null);

        var exception = assertThrows(FileNotFoundException.class, () -> cacheFileStore.readFile(hash));
        assertEquals(String.format(FILE_NOT_FOUND_EXCEPTION_MESSAGE, hash), exception.getMessage());
    }

    @Test
    void readFileTest() throws IOException {
        var hash = "hash";
        var content = "content";
        var expected = new ContentAccessorDirect(content);

        when(dataCache.getIfPresent(hash)).thenReturn(content);

        var result = cacheFileStore.readFile(hash);
        assertNotNull(result);
        assertEquals(expected, result);
    }

    @Test
    void replaceFileTest() throws IOException {
        var hash = "hash";
        var content = "content";
        var contentMock = mock(ContentAccessor.class);

        when(contentMock.getContentAsString()).thenReturn(content);
        doNothing().when(dataCache).put(eq(hash), eq(content));

        cacheFileStore.replaceFile(hash, contentMock);
        verify(dataCache).put(eq(hash), eq(content));
    }

    @Test
    void deleteFileTest() throws IOException {
        var hash = "hash";
        var mapSpy = spy(new ConcurrentHashMap<String, String>());

        mapSpy.put(hash, "existingContent");
        when(dataCache.asMap()).thenReturn(mapSpy);

        cacheFileStore.deleteFile(hash);
        verify(mapSpy).remove(hash);
    }

    @Test
    void deleteFileThrowsExceptionTest() {
        var hash = "hash";
        var mapSpy = spy(new ConcurrentHashMap<String, String>());

        when(dataCache.asMap()).thenReturn(mapSpy);
        var exception = assertThrows(FileNotFoundException.class, () -> cacheFileStore.deleteFile(hash));
        assertEquals(String.format(FILE_NOT_FOUND_EXCEPTION_MESSAGE, hash), exception.getMessage());
    }

    @Test
    void getFileIterableTest() {
        assertNull(cacheFileStore.getFileIterable());
    }

    @Test
    void clearStorageTest() throws IOException {
        cacheFileStore.clearStorage();
        verify(dataCache).invalidateAll();
    }

}

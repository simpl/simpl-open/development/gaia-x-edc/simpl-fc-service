package eu.xfsc.fc.core.util;

import java.util.HashSet;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.vocabulary.RDF;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SchemaStoreUtilsTest {

    @Test
    void addUrlsToSetForGivenNodeFromModelShouldPopulateGivenSet() {
        final var model = ModelFactory.createDefaultModel();
        final var resource1 = model.createResource("http://example.org/Resource1");
        final var resource2 = model.createResource("http://example.org/Resource2");
        final var node =  model.createResource("http://example.org/TestNode");

        // Add properties to associate the resources with the node
        model.add(resource1, RDF.type, node);
        model.add(resource2, RDF.type, node);

        final var result = new HashSet<String>();
        SchemaStoreUtils.addUrlsToSetForGivenNodeFromModel(model, node, result);

        // Assert that the extractedSet contains the URIs of the resources
        assertEquals(2, result.size());
        assertTrue(result.contains("http://example.org/Resource1"));
        assertTrue(result.contains("http://example.org/Resource2"));
    }
}
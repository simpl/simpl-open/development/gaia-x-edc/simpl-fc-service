package eu.xfsc.fc.core.service.filestore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import eu.xfsc.fc.core.pojo.ContentAccessor;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class FileStoreImplTest {

    @TempDir
    File tempDir;

    @InjectMocks
    private FileStoreImpl fileStore;

    @Mock
    private ContentAccessor contentAccessor;



    private final String storeName = "testStore";
    private final String childPath = "testStore/0c/b0/0cb0babc494fbaf69150baf8b73a9aa51d6e0d532d25f245660c8c8ac99c55d8";

    @BeforeEach
    void setUp() {
        fileStore = new FileStoreImpl(storeName);
        fileStore.setDirectoryTreeDepth(2);  // Set to a small depth for testing
        fileStore.setDirectoryNameLength(2); // Set to a small length for testing
        ReflectionTestUtils.setField(fileStore, "basePathName", tempDir.getAbsolutePath());
        ReflectionTestUtils.setField(fileStore, "scope", "prod");
    }

    @Test
    void storeFileShouldNotThrowException() {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";
        when(contentAccessor.getContentAsStream()).thenReturn(IOUtils.toInputStream("content", "UTF-8"));

        // When
        assertThatCode(() -> fileStore.storeFile(hash, contentAccessor))
                .doesNotThrowAnyException();
    }

    @Test
    void storeFileShouldThrowFileExistsExceptionWhenFileAlreadyExists() throws IOException {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";
        File existingFile = new File(tempDir, childPath);
        FileUtils.writeStringToFile(existingFile, "existing content", "UTF-8");

        // When / Then
        assertThatThrownBy(() -> fileStore.storeFile(hash, contentAccessor))
                .isInstanceOf(FileExistsException.class)
                .hasMessageContaining("A file for the hash " + hash + " already exists.");
    }

    @Test
    void replaceFileShouldReplaceFileWhenFileExists() throws IOException {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";
        File existingFile = new File(tempDir, childPath);
        FileUtils.writeStringToFile(existingFile, "existing content", "UTF-8");
        when(contentAccessor.getContentAsStream()).thenReturn(IOUtils.toInputStream("new content", "UTF-8"));

        // When
        fileStore.replaceFile(hash, contentAccessor);

        // Then
        assertThat(FileUtils.readFileToString(existingFile, "UTF-8")).isEqualTo("new content");
    }

    @Test
    void readFileShouldReturnContentAccessorWhenFileExists() throws IOException {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";
        File existingFile = new File(tempDir, childPath);
        FileUtils.writeStringToFile(existingFile, "existing content", "UTF-8");

        // When
        ContentAccessor contentAccessor = fileStore.readFile(hash);

        // Then
        assertThat(IOUtils.toString(contentAccessor.getContentAsStream(), "UTF-8")).isEqualTo("existing content");
    }

    @Test
    void readFileShouldThrowFileNotFoundExceptionWhenFileDoesNotExist() {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";

        // When / Then
        assertThatThrownBy(() -> fileStore.readFile(hash))
                .isInstanceOf(FileNotFoundException.class)
                .hasMessageContaining("A file for the hash " + hash + " does not exist.");
    }

    @Test
    void readFileValidateFileNameReturnsFilename() {
        String hash = "0cb0babc494fbaf69150baf8b73a9aa51d6e0d532d25f245660c8c8ac99c55d8";
        var result = fileStore.validateFileName(hash);
        assertNotNull(contentAccessor);
        assertEquals(hash, result);
    }

    @Test
    void deleteFileShouldDeleteFileWhenFileExists() throws IOException {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";
        File existingFile = new File(tempDir, childPath);
        FileUtils.writeStringToFile(existingFile, "existing content", "UTF-8");

        // When
        fileStore.deleteFile(hash);

        // Then
        assertThat(existingFile).doesNotExist();
    }

    @Test
    void deleteFileShouldThrowFileNotFoundExceptionWhenFileDoesNotExist() {
        // Given
        String hash = "abcd1234efgh5678ijkl9012mnop3456qrst7890";

        // When / Then
        assertThatThrownBy(() -> fileStore.deleteFile(hash))
                .isInstanceOf(FileNotFoundException.class)
                .hasMessageContaining("A file for the hash " + hash + " does not exist.");
    }

    @Test
    void clearStorageShouldDeleteAllFiles() throws IOException {
        // Given
        FileUtils.writeStringToFile(new File(tempDir, "/testStore/ab/cd/abcd1234efgh5678ijkl9012mnop3456qrst7890"), "content", "UTF-8");
        FileUtils.writeStringToFile(new File(tempDir, "/testStore/ef/gh/efgh5678ijkl9012mnop3456qrst7890abcd1234"), "content", "UTF-8");

        // When
        fileStore.clearStorage();

        // Then
        assertThat(tempDir).isEmptyDirectory();
    }

    @Test
    void getFileIterableShouldReturnAllFilesInStore() throws IOException {
        // Given
        FileUtils.writeStringToFile(new File(tempDir, "/testStore/ab/cd/abcd1234efgh5678ijkl9012mnop3456qrst7890"), "content", "UTF-8");
        FileUtils.writeStringToFile(new File(tempDir, "/testStore/ef/gh/efgh5678ijkl9012mnop3456qrst7890abcd1234"), "content", "UTF-8");

        // When
        Iterable<File> fileIterable = fileStore.getFileIterable();

        // Then
        Iterator<File> iterator = fileIterable.iterator();
        assertThat(iterator.hasNext()).isTrue();
        assertThat(iterator.next().getName()).isEqualTo("abcd1234efgh5678ijkl9012mnop3456qrst7890");
        assertThat(iterator.hasNext()).isTrue();
        assertThat(iterator.next().getName()).isEqualTo("efgh5678ijkl9012mnop3456qrst7890abcd1234");
        assertThat(iterator.hasNext()).isFalse();
    }

    @Test
    void hashFileIteratorShouldIterateOverFiles() throws IOException {
        // Given
        File file1 = new File(tempDir, childPath);
        FileUtils.writeStringToFile(file1, "content1", "UTF-8");

        // When
        FileStoreImpl.HashFileIterator iterator = new FileStoreImpl.HashFileIterator(fileStore, storeName);

        // Then
        assertThat(iterator.hasNext()).isTrue();
        assertThat(iterator.next().getName()).isEqualTo(file1.getName());
        assertThat(iterator.hasNext()).isFalse();
    }

    @Test
    void hashFileIteratorShouldThrowNoSuchElementException_whenNoMoreFiles() {
        // Given
        FileStoreImpl.HashFileIterator iterator = new FileStoreImpl.HashFileIterator(fileStore, storeName);

        // When / Then
        assertThatThrownBy(iterator::next).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void testGetAndSetDirectoryTreeDepth() {
        // Given
        fileStore.setDirectoryTreeDepth(3);

        // When
        int depth = fileStore.getDirectoryTreeDepth();

        // Then
        assertThat(depth).isEqualTo(3);
    }

    @Test
    void testGetAndSetDirectoryNameLength() {
        // Given
        fileStore.setDirectoryNameLength(4);

        // When
        int length = fileStore.getDirectoryNameLength();

        // Then
        assertThat(length).isEqualTo(4);
    }

    @Test
    void clearStorageTest() throws IOException {
        ReflectionTestUtils.setField(fileStore, "scope", "test");
        fileStore.clearStorage();
    }

}
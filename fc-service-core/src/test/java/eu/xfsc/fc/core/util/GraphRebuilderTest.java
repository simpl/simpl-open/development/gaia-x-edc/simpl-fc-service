package eu.xfsc.fc.core.util;

import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.pojo.SdClaim;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.service.graphdb.GraphStore;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.VerificationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class GraphRebuilderTest {

    @Mock
    private SelfDescriptionStore sdStore;

    @Mock
    private GraphStore graphStore;

    @Mock
    private VerificationService verificationService;

    @InjectMocks
    private GraphRebuilder graphRebuilder;

    @Test
    void rebuildGraphDbTest() {

        var chunkCount = 1;
        var chunkId = 0;
        var threads = 1;
        var batchSize = 2;
        var hash1 = "hash1";
        var hash2 = "hash2";
        var metadata1 = mock(SelfDescriptionMetadata.class);
        var metadata2 = mock(SelfDescriptionMetadata.class);
        var content1 = mock(ContentAccessor.class);
        var content2 = mock(ContentAccessor.class);
        var claim1 = mock(SdClaim.class);
        var claim2 = mock(SdClaim.class);

        when(sdStore.getActiveSdHashes(null, batchSize, chunkCount, chunkId))
                .thenReturn(List.of(hash1, hash2))
                .thenReturn(Collections.emptyList());

        when(sdStore.getByHash(hash1)).thenReturn(metadata1);
        when(sdStore.getByHash(hash2)).thenReturn(metadata2);

        when(metadata1.getSelfDescription()).thenReturn(content1);
        when(metadata2.getSelfDescription()).thenReturn(content2);

        when(verificationService.extractClaims(content1))
                .thenReturn(List.of(claim1));
        when(verificationService.extractClaims(content2))
                .thenReturn(List.of(claim2));

        graphRebuilder.rebuildGraphDb(chunkCount, chunkId, threads, batchSize);

        verify(graphStore, times(1)).addClaims(eq(List.of(claim1)), any());
        verify(graphStore, times(1)).addClaims(eq(List.of(claim2)), any());
    }

}

package eu.xfsc.fc.core.service.pubsub.event;

import eu.xfsc.fc.api.generated.model.SelfDescriptionStatus;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class UpdateEventHandlerTest {

    @Mock
    private SelfDescriptionStore sdStore;

    @InjectMocks
    private UpdateEventHandler updateEventHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this); // Initializes mocks
    }

    @Test
    void testHandleWithValidParams() {
        // Arrange
        String hash = "someHash";
        String status = "ACTIVE";
        Map<String, Object> params = new HashMap<>();
        params.put("hash", hash);
        params.put("status", status);

        // Act
        updateEventHandler.handle(params);

        // Assert
        ArgumentCaptor<String> hashCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<SelfDescriptionStatus> statusCaptor = ArgumentCaptor.forClass(SelfDescriptionStatus.class);

        verify(sdStore, times(1)).changeLifeCycleStatus(hashCaptor.capture(), statusCaptor.capture());

        assertEquals(hash, hashCaptor.getValue());
        assertEquals(SelfDescriptionStatus.ACTIVE, statusCaptor.getValue());
    }
}
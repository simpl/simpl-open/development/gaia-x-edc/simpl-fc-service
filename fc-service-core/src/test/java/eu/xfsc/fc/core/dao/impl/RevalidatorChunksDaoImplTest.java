package eu.xfsc.fc.core.dao.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RevalidatorChunksDaoImplTest {

    @Mock
    private JdbcTemplate jdbc;

    @InjectMocks
    private RevalidatorChunksDaoImpl revalidatorChunksDao;

    @Test
    void getConnectionTest() throws SQLException {
        var dataSourceMock = mock(DataSource.class);
        var connectionMock = mock(Connection.class);

        when(jdbc.getDataSource()).thenReturn(dataSourceMock);
        when(dataSourceMock.getConnection()).thenReturn(connectionMock);

        var result = revalidatorChunksDao.getConnection();

        assertNotNull(result);
        assertEquals(connectionMock, result);
    }

    @Test
    void getConnectionNullTest() throws SQLException {
        when(jdbc.getDataSource()).thenReturn(null);

        assertThrows(NullPointerException.class, () -> revalidatorChunksDao.getConnection());
    }

}

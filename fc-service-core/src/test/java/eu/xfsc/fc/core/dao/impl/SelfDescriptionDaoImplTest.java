package eu.xfsc.fc.core.dao.impl;

import eu.xfsc.fc.core.service.sdstore.SdMetaRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SelfDescriptionDaoImplTest {

    @Mock
    private NamedParameterJdbcTemplate jdbc;

    @InjectMocks
    private SelfDescriptionDaoImpl selfDescriptionDaoImpl;

    @Test
    void selectByIdShouldReturnNullWhenJdbcThrowsEmptyResultDataAccessException() {
        doThrow(new EmptyResultDataAccessException(1))
                .when(jdbc).queryForObject(anyString(), any(SqlParameterSource.class), any(RowMapper.class));

        final var result = selfDescriptionDaoImpl.selectById("id");

        assertThat(result).isNull();
    }

    @Test
    void selectByIdShouldReturnRecordWhenNoExceptionIsThrown() {
        final var sdmr = Mockito.mock(SdMetaRecord.class);
        when(jdbc.queryForObject(anyString(), any(SqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(sdmr);

        final var result = selfDescriptionDaoImpl.selectById("id");

        assertThat(result).isNotNull();
    }
}
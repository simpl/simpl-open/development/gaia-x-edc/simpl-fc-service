package eu.xfsc.fc.core.service.verification.ontology;

import lombok.Getter;

public class OntologyDataFactory {

    public static final String ONTOLOGY = """
            @prefix simpl:
            <http://w3id.org/gaia-x/simpl#> .
            @prefix gax-core:
                <https://w3id.org/gaia-x/core#> .
            @prefix vcard:
                    <https://www.w3.org/2006/vcard/ns#> .
            @prefix foaf:
                        <http://xmlns.com/foaf/0.1/> .
            @prefix vcard:
                        <http://www.w3.org/2006/vcard/ns#> .
            @prefix did:
                            <https://www.w3.org/TR/did-core/#> .
            @prefix xsd:
                                <http://www.w3.org/2001/XMLSchema#> .
            @prefix dct:
                                    <http://purl.org/dc/terms/> .
            @prefix dcat:
                                    <http://www.w3.org/ns/dcat#> .
            @prefix cc:
                                        <http://creativecommons.org/ns#> .
            @prefix ids:
                                            <https://w3id.org/idsa/core/> .
            @prefix owl:
                                            <http://www.w3.org/2002/07/owl#> .
            @prefix rdf:
                                                <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
            @prefix rdfs:
                                                    <http://www.w3.org/2000/01/rdf-schema#> .
            @prefix skos:
                                                        <http://www.w3.org/2004/02/skos/core#> .
            @prefix vann:
                                                            <http://purl.org/vocab/vann/> .
            @prefix voaf:
                                                            <http://purl.org/vocommons/voaf#> .
            @prefix void:
                                                                <http://rdfs.org/ns/void#> .
            @prefix schema:
                                                                    <http://schema.org/> .
                        
                        
            simpl:
            a		voaf:Vocabulary, owl:Ontology ;
            rdfs:label		"Gaia-X trusted-cloud Ontology"@en ;
            dct:title		"Gaia-X trusted-cloud Ontology"@en ;
            dct:abstract		"todo" ;
            cc:license
                                                                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            dct:creator		"Working Group Service Characteristics" ;
            dct:contributor		"Akyürek, Haydar", "Bader, Sebastian", "Baum, Hannes", "Blanch, Josep", "Frömberg, Jan", "Gronlier, Pierre", "Hermsen, Felix", "Lange, Christoph", "Langkau, Jörg", "Leberecht, Markus", "Meinke, Kai", "Moosmann, Paul", "Niessen, Thomas", "Ogel, Frederic", "Ottradovetz, Klaus", "Qin, Chang", "Rubina, Alina", "Staginus, Judith", "Strunk, Anja", "Theissen-Lipp, Johannes" ;
            dct:created		"2021-10-18T12:00:00+01:00"^^xsd:dateTimeStamp ;
            owl:versionInfo		"22.04" ;
            vann:preferredNamespaceUri		"https://simpl#" ;
            vann:preferredNamespacePrefix		"simpl" ;
            void:vocabulary 		vann:, void:, voaf:, dct: ;
            dct:modified		"2024-06-10T17:32:02+02:00"^^xsd:dateTimeStamp ;
            .
                        
            ##################
            ##ApplicationProperties
            ##################
                        
            simpl:ApplicationProperties
            	a		 owl:Class;
            	rdfs:label		 "Application Properties"@en ;
            .
                        
            simpl:package
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "package"@en ;
            	rdfs:domain		 simpl:ApplicationProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Application package" ;
            .
                        
            simpl:link
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "link"@en ;
            	rdfs:domain		 simpl:ApplicationProperties ;
            	rdfs:range		 xsd:anyURI ;
            	rdfs:comment		 "Application link" ;
            .
                        
            simpl:format
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "format"@en ;
            	rdfs:domain		 simpl:ApplicationProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Format under which the application is distributed (e.g. docker image, executable package, ...). Might be more than one format available for the same application." ;
            .
                        
            simpl:target
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "target"@en ;
            	rdfs:domain		 simpl:ApplicationProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Target users" ;
            .
                        
            simpl:requirements
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "requirements"@en ;
            	rdfs:domain		 simpl:ApplicationProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Requirements on the runtime environment (useful for the deployment in the infrastructure provider)" ;
            .
                        
            simpl:url
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "url"@en ;
            	rdfs:domain		 simpl:ApplicationProperties ;
            	rdfs:range		 xsd:anyURI ;
            	rdfs:comment		 "URL of the documentation on how to use the application" ;
            .
                        
            ##################
            ##DataProperties
            ##################
                        
            simpl:DataProperties
            	a		 owl:Class;
            	rdfs:label		 "Data Properties"@en ;
            .
                        
            simpl:provenance
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "provenance"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Data Provenance" ;
            .
                        
            simpl:format
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "format"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Format under which the data is distributed (e.g. csv, xml, …). Might be more than one format available for the same dataset." ;
            .
                        
            simpl:schema
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "schema"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Schema of the dataset: thus, a description what the structure of the data looks like. What are the columns and the data ranges." ;
            .
                        
            simpl:related
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "related"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Related datasets" ;
            .
                        
            simpl:target
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "target"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Target users" ;
            .
                        
            simpl:quality
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "quality"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Data Quality (to include metrics such as completeness, accuracy, timeliness and other)" ;
            .
                        
            simpl:encryption
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "encryption"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Encryption: Describes the encryption algorithms and keys used to secure the data" ;
            .
                        
            simpl:anonymization
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "anonymization"@en ;
            	rdfs:domain		 simpl:DataProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Anonymization/pseudonymization: Indicates whether sensitive information has been anonymized or pseudonymized to protect privacy" ;
            .
                        
            ##################
            ##GeneralServiceProperties
            ##################
                        
            simpl:GeneralServiceProperties
            	a		 owl:Class;
            	rdfs:label		 "General Service Properties"@en ;
            .
                        
            simpl:identifier
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "identifier"@en ;
            	rdfs:domain		 simpl:GeneralServiceProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "An identifier that is unique over the catalogue for instance URI" ;
            .
                        
            simpl:title
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "title"@en ;
            	rdfs:domain		 simpl:GeneralServiceProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "A short text title for the dataset/application/infrastructure" ;
            .
                        
            simpl:description
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "description"@en ;
            	rdfs:domain		 simpl:GeneralServiceProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "A description for the dataset/application/infrastructure" ;
            .
                        
            simpl:location
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "location"@en ;
            	rdfs:domain		 simpl:GeneralServiceProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "The location where the dataset/application/infrastructure can be found (likely an endpoint URL)" ;
            .
                        
            simpl:keywords
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "keywords"@en ;
            	rdfs:domain		 simpl:GeneralServiceProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Keywords" ;
            .
                        
            simpl:language
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "language"@en ;
            	rdfs:domain		 simpl:GeneralServiceProperties ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Language (of the metadata, like the title, description)" ;
            .
                        
            ##################
            ##OfferingPrice
            ##################
                        
            simpl:OfferingPrice
            	a		 owl:Class;
            	rdfs:label		 "Offering Price"@en ;
            .
                        
            simpl:license
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "license"@en ;
            	rdfs:domain		 simpl:OfferingPrice ;
            	rdfs:range		 xsd:anyURI ;
            	rdfs:comment		 "URL to the used license" ;
            .
                        
            simpl:price
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "price"@en ;
            	rdfs:domain		 simpl:OfferingPrice ;
            	rdfs:range		 xsd:integer ;
            	rdfs:comment		 "Price and conditions" ;
            .
                        
            ##################
            ##ProviderInformation
            ##################
                        
            simpl:ProviderInformation
            	a		 owl:Class;
            	rdfs:label		 "Provider Information"@en ;
            .
                        
            simpl:provider
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "provider"@en ;
            	rdfs:domain		 simpl:ProviderInformation ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "A reference to ID of the Data/Application/Infrastructure Provider" ;
            .
                        
            simpl:contact
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "contact"@en ;
            	rdfs:domain		 simpl:ProviderInformation ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Who to contact in case of questions/issues" ;
            .
                        
            simpl:signature
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "signature"@en ;
            	rdfs:domain		 simpl:ProviderInformation ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "A digital signature that ensures that the provider is the one that uploaded the Self-description" ;
            .
                        
            ##################
            ##ServicePolicy
            ##################
                        
            simpl:ServicePolicy
            	a		 owl:Class;
            	rdfs:label		 "Service Policy"@en ;
            .
                        
            simpl:access-policy
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "access policy"@en ;
            	rdfs:domain		 simpl:ServicePolicy ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Access policy to define who can access the dataset" ;
            .
                        
            simpl:usage-policy
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "usage policy"@en ;
            	rdfs:domain		 simpl:ServicePolicy ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Usage policy to define how a dataset can be used" ;
            .
                        
            simpl:compliance
            	a		 owl:DatatypeProperty ;
            	rdfs:label		 "compliance"@en ;
            	rdfs:domain		 simpl:ServicePolicy ;
            	rdfs:range		 xsd:string ;
            	rdfs:comment		 "Indicates compliance with relevant data protection regulations and standards" ;
            .
                        
            ##################
            ##ApplicationOffering
            ##################
                        
            simpl:ApplicationOffering
            	a		 owl:Class;
            	rdfs:label		 "Application Offering"@en ;
            	rdfs:subClassOf		simpl:ServiceOffering ;
            .
                        
            simpl:applicationProperties
            	a		 owl:ObjectProperty ;
            	rdfs:label		 "application properties"@en ;
            	rdfs:domain		 simpl:ApplicationOffering ;
            	rdfs:range		 simpl:ApplicationProperties ;
            	rdfs:comment		 "Basic classification of the service as per provision type and service model." ;
            .
                        
            ##################
            ##DataOffering
            ##################
                        
            simpl:DataOffering
            	a		 owl:Class;
            	rdfs:label		 "Data Offering"@en ;
            	rdfs:subClassOf		simpl:ServiceOffering ;
            .
                        
            simpl:dataProperties
            	a		 owl:ObjectProperty ;
            	rdfs:label		 "data properties"@en ;
            	rdfs:domain		 simpl:DataOffering ;
            	rdfs:range		 simpl:DataProperties ;
            	rdfs:comment		 "Basic classification of the service as per provision type and service model." ;
            .
                        
            ##################
            ##ServiceOffering
            ##################
                        
            simpl:ServiceOffering
            	a		 owl:Class;
            	rdfs:label		 "Service Offering"@en ;
            	rdfs:subClassOf		gax-core:ServiceOffering ;
            .
                        
            simpl:generalServiceProperties
            	a		 owl:ObjectProperty ;
            	rdfs:label		 "general service properties"@en ;
            	rdfs:domain		 simpl:ServiceOffering ;
            	rdfs:range		 simpl:GeneralServiceProperties ;
            	rdfs:comment		 "Basic classification of the service as per provision type and service model." ;
            .
                        
            simpl:providerInformation
            	a		 owl:ObjectProperty ;
            	rdfs:label		 "provider information"@en ;
            	rdfs:domain		 simpl:ServiceOffering ;
            	rdfs:range		 simpl:ProviderInformation ;
            	rdfs:comment		 "Basic classification of the service as per provision type and service model." ;
            .
                        
            simpl:offeringPrice
            	a		 owl:ObjectProperty ;
            	rdfs:label		 "offering price"@en ;
            	rdfs:domain		 simpl:ServiceOffering ;
            	rdfs:range		 simpl:OfferingPrice ;
            	rdfs:comment		 "Basic classification of the service as per provision type and service model." ;
            .
                        
            simpl:servicePolicy
            	a		 owl:ObjectProperty ;
            	rdfs:label		 "service policy"@en ;
            	rdfs:domain		 simpl:ServiceOffering ;
            	rdfs:range		 simpl:ServicePolicy ;
            	rdfs:comment		 "Basic classification of the service as per provision type and service model." ;
            .
            """;

    public static String VALID_PAYLOAD = """
            {
              "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1"
              ],
              "credentialSubject": {
                "@context": {
                  "gax-validation": "http://w3id.org/gaia-x/validation#",
                  "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                  "sh": "http://www.w3.org/ns/shacl#",
                  "simpl": "http://w3id.org/gaia-x/simpl#",
                  "skos": "http://www.w3.org/2004/02/skos/core#",
                  "xsd": "http://www.w3.org/2001/XMLSchema#"
                },
                "@id": "did:web:registry.gaia-x.eu:DataOffering:PW9tk7FoGBNz3zPbIULGiRsmgBUc3eACgYir",
                "@type": "simpl:DataOffering",
                "simpl:dataProperties": {
                  "@type": "simpl:DataProperties",
                  "simpl:format": {
                    "@type": "xsd:string",
                    "@value": "xml"
                  }
                },
                "simpl:generalServiceProperties": {
                  "@type": "simpl:GeneralServiceProperties",
                  "simpl:description": {
                    "@type": "xsd:string",
                    "@value": "descriptionDemo"
                  },
                  "simpl:identifier": {
                    "@type": "xsd:string",
                    "@value": "identifeirDemo"
                  },
                  "simpl:location": {
                    "@type": "xsd:string",
                    "@value": "locationDemo"
                  },
                  "simpl:title": {
                    "@type": "xsd:string",
                    "@value": "titleDemo"
                  }
                },
                "simpl:offeringPrice": {
                  "@type": "simpl:OfferingPrice",
                  "simpl:license": {
                    "@type": "xsd:anyURI",
                    "@value": "licenseDemo"
                  },
                  "simpl:price": {
                    "@type": "xsd:decimal",
                    "@value": "1000"
                  }
                },
                "simpl:providerInformation": {
                  "@type": "simpl:ProviderInformation",
                  "simpl:contact": {
                    "@type": "xsd:string",
                    "@value": "contactDemo"
                  },
                  "simpl:provider": {
                    "@type": "xsd:string",
                    "@value": "providerDemo"
                  }
                },
                "simpl:servicePolicy": {
                  "@type": "simpl:ServicePolicy"
                }
              },
              "issuanceDate": "2024-06-21T12:58:29.615994702Z",
              "issuer": "did:web:did.dev.simpl-europa.eu",
              "proof": {
                "created": "2024-06-21T12:58:29.624380198Z",
                "jws": "eyJhbGciOiJVbmRlZmluZWQiLCJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdfQ..Fq9B2aJ4rAU-ugQD9zUp26ysm_ssTO2UaKmJ5jiYEdriW5bRlAVT_ISoncwoQPmOuzBf3qLPYtPFj-ZX_rnUDg",
                "proofPurpose": "assertionMethod",
                "type": "JsonWebSignature2020",
                "verificationMethod": "did:web:did.dev.simpl-europa.eu#gaia-x-key1"
              },
              "type": "VerifiableCredential"
                        
                        
             \s
            }
            """;

    public static String INVALID_PAYLOAD_RANGE_MISMATCH = """
            {
              "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1"
              ],
              "credentialSubject": {
                "@context": {
                  "gax-validation": "http://w3id.org/gaia-x/validation#",
                  "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                  "sh": "http://www.w3.org/ns/shacl#",
                  "simpl": "http://w3id.org/gaia-x/simpl#",
                  "skos": "http://www.w3.org/2004/02/skos/core#",
                  "xsd": "http://www.w3.org/2001/XMLSchema#"
                },
                "@id": "did:web:registry.gaia-x.eu:DataOffering:PW9tk7FoGBNz3zPbIULGiRsmgBUc3eACgYir",
                "@type": "simpl:DataOffering",
                "simpl:dataProperties": {
                  "@type": "simpl:DataProperties",
                  "simpl:format": {
                    "@type": "xsd:string",
                    "@value": "xml"
                  }
                },
                "simpl:generalServiceProperties": {
                  "@type": "simpl:GeneralServiceProperties",
                  "simpl:description": {
                    "@type": "xsd:string",
                    "@value": "descriptionDemo"
                  },
                  "simpl:identifier": {
                    "@type": "xsd:string",
                    "@value": "identifeirDemo"
                  },
                  "simpl:location": {
                    "@type": "xsd:string",
                    "@value": "locationDemo"
                  },
                  "simpl:title": {
                    "@type": "xsd:string",
                    "@value": "titleDemo"
                  }
                },
                "simpl:offeringPrice": {
                  "@type": "simpl:OfferingPrice",
                  "simpl:license": {
                    "@type": "xsd:anyURI",
                    "@value": "licenseDemo"
                  },
                  "simpl:price": {
                    "@type": "xsd:decimal",
                    "@value": "1000"
                  }
                },
                "simpl:providerInformation": {
                  "@type": "simpl:ProviderInformation",
                  "simpl:contact": {
                    "@type": "xsd:string",
                    "@value": "contactDemo"
                  },
                  "simpl:provider": {
                    "@type": "xsd:Data",
                    "@value": "providerDemo"
                  }
                },
                "simpl:servicePolicy": {
                  "@type": "simpl:ServicePolicy"
                }
              },
              "issuanceDate": "2024-06-21T12:58:29.615994702Z",
              "issuer": "did:web:did.dev.simpl-europa.eu",
              "proof": {
                "created": "2024-06-21T12:58:29.624380198Z",
                "jws": "eyJhbGciOiJVbmRlZmluZWQiLCJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdfQ..Fq9B2aJ4rAU-ugQD9zUp26ysm_ssTO2UaKmJ5jiYEdriW5bRlAVT_ISoncwoQPmOuzBf3qLPYtPFj-ZX_rnUDg",
                "proofPurpose": "assertionMethod",
                "type": "JsonWebSignature2020",
                "verificationMethod": "did:web:did.dev.simpl-europa.eu#gaia-x-key1"
              },
              "type": "VerifiableCredential"
                        
                        
             \s
            }
            
            """;

    public static String INVALID_PAYLOAD_DOMAIN_MISMATCH = """
              {
              "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1"
              ],
              "credentialSubject": {
                "@context": {
                  "gax-validation": "http://w3id.org/gaia-x/validation#",
                  "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                  "sh": "http://www.w3.org/ns/shacl#",
                  "simpl": "http://w3id.org/gaia-x/simpl#",
                  "skos": "http://www.w3.org/2004/02/skos/core#",
                  "xsd": "http://www.w3.org/2001/XMLSchema#"
                },
                "@id": "did:web:registry.gaia-x.eu:DataOffering:PW9tk7FoGBNz3zPbIULGiRsmgBUc3eACgYir",
                "@type": "simpl:DataOffering",
                "simpl:dataProperties": {
                  "@type": "simpl:DataProperties",
                  "simpl:format": {
                    "@type": "xsd:string",
                    "@value": "xml"
                  }
                },
                "simpl:generalServiceProperties": {
                  "@type": "simpl:GeneralServiceProperties",
                  "simpl:description": {
                    "@type": "xsd:string",
                    "@value": "descriptionDemo"
                  },
                  "simpl:location": {
                    "@type": "xsd:string",
                    "@value": "locationDemo"
                  },
                  "simpl:title": {
                    "@type": "xsd:string",
                    "@value": "titleDemo"
                  }
                },
                "simpl:offeringPrice": {
                  "@type": "simpl:OfferingPrice",
                  "simpl:license": {
                    "@type": "xsd:anyURI",
                    "@value": "licenseDemo"
                  },
                  "simpl:identifier": {
                    "@type": "xsd:string",
                    "@value": "identifeirDemo"
                  },
                  "simpl:price": {
                    "@type": "xsd:decimal",
                    "@value": "1000"
                  }
                },
                "simpl:providerInformation": {
                  "@type": "simpl:ProviderInformation",
                  "simpl:contact": {
                    "@type": "xsd:string",
                    "@value": "contactDemo"
                  },
                  "simpl:provider": {
                    "@type": "xsd:string",
                    "@value": "providerDemo"
                  }
                },
                "simpl:servicePolicy": {
                  "@type": "simpl:ServicePolicy"
                }
              },
              "issuanceDate": "2024-06-21T12:58:29.615994702Z",
              "issuer": "did:web:did.dev.simpl-europa.eu",
              "proof": {
                "created": "2024-06-21T12:58:29.624380198Z",
                "jws": "eyJhbGciOiJVbmRlZmluZWQiLCJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdfQ..Fq9B2aJ4rAU-ugQD9zUp26ysm_ssTO2UaKmJ5jiYEdriW5bRlAVT_ISoncwoQPmOuzBf3qLPYtPFj-ZX_rnUDg",
                "proofPurpose": "assertionMethod",
                "type": "JsonWebSignature2020",
                "verificationMethod": "did:web:did.dev.simpl-europa.eu#gaia-x-key1"
              },
              "type": "VerifiableCredential"
                        
                        
             \s
            }          
            """;
}

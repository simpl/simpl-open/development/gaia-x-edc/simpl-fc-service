package eu.xfsc.fc.core.service.graphdb;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.xfsc.fc.api.generated.model.QueryLanguage;
import eu.xfsc.fc.core.exception.ServerException;
import eu.xfsc.fc.core.exception.TimeoutException;
import eu.xfsc.fc.core.pojo.GraphQuery;
import eu.xfsc.fc.core.pojo.PaginatedResults;
import eu.xfsc.fc.core.pojo.SdClaim;
import eu.xfsc.fc.core.util.ClaimValidator;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.exceptions.Neo4jException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class Neo4jGraphStoreTest {
    @Mock
    private Driver driver;

    @Mock
    private Session session;

    @Mock
    private ClaimValidator claimValidator;

    @InjectMocks
    private Neo4jGraphStore neo4jGraphStore;

    @BeforeEach
    void setUp() throws NoSuchFieldException, IllegalAccessException {
        Field field = Neo4jGraphStore.class.getDeclaredField("claimValidator");
        field.setAccessible(true);
        field.set(neo4jGraphStore, claimValidator);
    }


    @Test
    void addClaimsShouldNotThrowException() {
        final var mockResultInUpdateGraphConfig = mock(Result.class);
        when(mockResultInUpdateGraphConfig.hasNext())
                .thenReturn(true, false);

        final var record = mock( org.neo4j.driver.Record.class);
        final Map<String, Object> recordAsMap = Map.of("param", "multivalPropList", "value", Set.of("lala"));
        when(record.asMap())
                .thenReturn(recordAsMap);
        when(mockResultInUpdateGraphConfig.next())
                .thenReturn(record);
        final var mockResultInAddClaims = mock(Result.class);

        when(driver.session()).thenReturn(session);
        when(session.run(anyString(), anyMap()))
                .thenReturn(mockResultInAddClaims);
        when(session.run(anyString()))
                .thenReturn(mockResultInUpdateGraphConfig);

        String credentialSubject = "subjecto1";
        List<SdClaim> claims = List.of(new SdClaim(credentialSubject, "predicate", "object"));
        Pair<String, Set<String>> props = Pair.of("payload", Set.of("property1", "property2"));
        when(claimValidator.resolveClaims(any(), any()))
                .thenReturn(props);

        assertThatCode(() -> neo4jGraphStore.addClaims(claims, credentialSubject))
                .doesNotThrowAnyException();

    }

    @Test
    void addClaimsClaimListIsEmpty() {

        neo4jGraphStore.addClaims(Collections.emptyList(), "test");
        verify(driver, never()).session();
        verify(claimValidator, never()).resolveClaims(any(), any());
    }

    @Test
    void addClaimsPropsRightIsEmpty() {

        String credentialSubject = "subjecto1";
        List<SdClaim> claims = List.of(new SdClaim(credentialSubject, "predicate", "object"));
        Pair<String, Set<String>> props = Pair.of("payload", Collections.emptySet());
        Result resultMock = mock(Result.class);

        when(driver.session()).thenReturn(session);
        when(claimValidator.resolveClaims(any(), any()))
                .thenReturn(props);
        when(session.run(anyString(), anyMap()))
                .thenReturn(resultMock);

        neo4jGraphStore.addClaims(claims, credentialSubject);
        verify(driver).session();
        verify(claimValidator).resolveClaims(any(), any());
        verify(session).run(eq("CALL n10s.rdf.import.inline($payload, \"N-Triples\");"), eq(Map.of("payload", props.getLeft())));
    }

    @Test
    void deleteClaimsShouldNotThrowException() {
        String credentialSubject = "subject";
        final var mockResult = mock(Result.class);

        when(driver.session()).thenReturn(session);
        when(session.run(eq("MATCH (n {claimsGraphUri: [$uri]})\nDETACH DELETE n;"), anyMap()))
                .thenReturn(mockResult);
        when(session.run(eq("MATCH (n) WHERE $uri IN n.claimsGraphUri\nSET n.claimsGraphUri = [g IN n.claimsGraphUri WHERE g <> $uri];"), anyMap()))
                .thenReturn(mockResult);

        neo4jGraphStore.deleteClaims(credentialSubject);

        verify(mockResult, times(2)).consume();
    }

    @Test
    @MockitoSettings(strictness = Strictness.LENIENT)
    void queryDataShouldNotThrowException() {

        GraphQuery graphQuery = mock(GraphQuery.class);
        when(graphQuery.getQueryLanguage()).thenReturn(QueryLanguage.OPENCYPHER);
        when(graphQuery.getTimeout()).thenReturn(0);
        when(graphQuery.isWithTotalCount()).thenReturn(false);
        when(graphQuery.getQuery()).thenReturn("MATCH (n) RETURN n");
        final PaginatedResults<String> paginatedResult = new PaginatedResults<>(List.of("lala"));
        when(driver.session()).thenReturn(session);
        when(session.executeRead(any(), any()))
                .thenReturn(paginatedResult);

        final var result = neo4jGraphStore.queryData(graphQuery);
        assertThat(result.getTotalCount()).isEqualTo(1);
    }

    @Test
    @MockitoSettings(strictness = Strictness.LENIENT)
    void queryDataShouldThrowUnsupportedLanguage() {
        // Arrange
        GraphQuery graphQuery = mock(GraphQuery.class);
        when(driver.session()).thenReturn(session);
        when(graphQuery.getQueryLanguage()).thenReturn(QueryLanguage.SPARQL);

        // Act & Assert
        assertThatThrownBy(() -> neo4jGraphStore.queryData(graphQuery))
                .isInstanceOf(UnsupportedOperationException.class)
                .hasMessageContaining("SPARQL query language is not supported yet");
    }

    @Test
    @Disabled(value = "sometimes test execution is to fast and stamp = 0 and query timeout even if it set also to 0 does " +
            "not allow to satisfy condition to throw this exception. so in the end sometimes it pass and sometimes it does not")
    @MockitoSettings(strictness = Strictness.LENIENT)
    void queryDataShouldThrowTimeoutException() {
        ReflectionTestUtils.setField(neo4jGraphStore, "timeoutMarker", "timeout");

        GraphQuery graphQuery = mock(GraphQuery.class);
        when(driver.session()).thenReturn(session);
        when(graphQuery.getQueryLanguage()).thenReturn(QueryLanguage.OPENCYPHER);
        when(graphQuery.getTimeout()).thenReturn(0);
        when(graphQuery.isWithTotalCount()).thenReturn(false);
        when(graphQuery.getQuery()).thenReturn("MATCH (n) RETURN n");

        doThrow(new Neo4jException("timeout error")).when(session).executeRead(any(), any());

        assertThatThrownBy(() -> neo4jGraphStore.queryData(graphQuery))
                .isInstanceOf(TimeoutException.class)
                .hasMessageContaining("query timeout");
    }

    @Test
    @MockitoSettings(strictness = Strictness.LENIENT)
    void queryDataShouldThrowServerException() {
        ReflectionTestUtils.setField(neo4jGraphStore, "timeoutMarker", "xyz");

        GraphQuery graphQuery = mock(GraphQuery.class);
        when(driver.session()).thenReturn(session);
        when(graphQuery.getQueryLanguage()).thenReturn(QueryLanguage.OPENCYPHER);
        when(graphQuery.getTimeout()).thenReturn(0);
        when(graphQuery.isWithTotalCount()).thenReturn(false);
        when(graphQuery.getQuery()).thenReturn("MATCH (n) RETURN n");

        doThrow(new Neo4jException("timeout error")).when(session).executeRead(any(), any());

        assertThatThrownBy(() -> neo4jGraphStore.queryData(graphQuery))
                .isInstanceOf(ServerException.class)
                .hasMessageContaining("error querying data");
    }
}

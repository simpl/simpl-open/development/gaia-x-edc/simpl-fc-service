package eu.xfsc.fc.core.service.schemastore.schematypestrategy;

import java.util.HashSet;

import org.apache.jena.rdf.model.ModelFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatCode;

@ExtendWith(MockitoExtension.class)
class ShapeUrlsExtractorTest {

    @InjectMocks
    private ShapeUrlsExtractor shapeUrlsExtractor;

    @Test
    void addUrlsFromModelToSetShouldNotThrowException() {
        final var model = ModelFactory.createDefaultModel();
        final var extractedUrls = new HashSet<String>();
        assertThatCode(() -> shapeUrlsExtractor.addUrlsFromModelToSet(model, extractedUrls))
                .doesNotThrowAnyException();
    }
}
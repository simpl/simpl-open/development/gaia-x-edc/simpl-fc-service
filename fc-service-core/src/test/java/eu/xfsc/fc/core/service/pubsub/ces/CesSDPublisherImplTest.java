package eu.xfsc.fc.core.service.pubsub.ces;

import java.util.concurrent.ExecutorService;

import eu.xfsc.fc.api.generated.model.SelfDescriptionStatus;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.pojo.VerificationResult;
import eu.xfsc.fc.core.service.pubsub.SDPublisher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CesSDPublisherImplTest {

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private ExecutorService executorService;

    @Mock
    private CompRestClient compClient;

    @Mock
    private CesRestClient cesClient;


    @InjectMocks
    private CesSDPublisherImpl cesSDPublisher;

    @Test
    void publishShouldPublishInNewThreadAndReturnTrueWhenNoTransactional() {
        final var selfDescriptionMetadata = new SelfDescriptionMetadata();
        final var verificationResult = mock(VerificationResult.class);

        final var result = cesSDPublisher.publish(selfDescriptionMetadata, verificationResult);

        verify(executorService).execute(any());
        assertThat(result).isTrue();
    }

    @Test
    void publishShouldPublishInOldThreadAndReturnFalseWhenTransactional() {
        ReflectionTestUtils.setField(cesSDPublisher, "transactional", true);

        final var selfDescriptionMetadata = new SelfDescriptionMetadata();
        selfDescriptionMetadata.setSelfDescription(new ContentAccessorDirect("blabla"));
        final var verificationResult = mock(VerificationResult.class);

        final var result = cesSDPublisher.publish(selfDescriptionMetadata, verificationResult);

        verify(executorService, never()).execute(any());
        assertThat(result).isFalse();
    }

    @Test
    void publishWithHashShouldNotPublishAnythingBecauseItDOesNotSupportStatusUpdate() {
        ReflectionTestUtils.setField(cesSDPublisher, "transactional", false);

        final var hash = "hash";
        final var sdeEvent = SDPublisher.SDEvent.ADD;
        final var selfDescriptionStatus = SelfDescriptionStatus.ACTIVE;

        final var result = cesSDPublisher.publish(hash, sdeEvent, selfDescriptionStatus);

        verify(executorService, never()).execute(any());
        assertThat(result).isFalse();
    }
}
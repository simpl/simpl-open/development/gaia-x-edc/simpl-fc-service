package eu.xfsc.fc.core.dao.impl;

import java.sql.Timestamp;
import java.time.Instant;

import eu.xfsc.fc.core.pojo.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ValidatorCacheDaoImplTest {

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private ValidatorCacheDaoImpl validatorCacheDao;

    @Test
    void getFromCacheShouldReturnValidValidator() {
        String didURI = "did:example:123";
        Validator expectedValidator = new Validator(didURI, "publicKey", Instant.now());

        when(jdbcTemplate.queryForObject(
                eq("select diduri, publickey, expirationtime from validatorcache where diduri = ?"),
                eq(new Object[]{didURI}),
                eq(new int[]{java.sql.Types.VARCHAR}),
                any(RowMapper.class))
        ).thenReturn(expectedValidator);

        Validator actualValidator = validatorCacheDao.getFromCache(didURI);

        assertThat(actualValidator).isNotNull();
        assertThat(actualValidator.getDidURI()).isEqualTo(expectedValidator.getDidURI());
        assertThat(actualValidator.getPublicKey()).isEqualTo(expectedValidator.getPublicKey());
        assertThat(actualValidator.getExpirationDate()).isEqualTo(expectedValidator.getExpirationDate());
    }

    @Test
    void getFromCacheShouldReturnNullWhenEmptyResultThrown() {
        String didURI = "did:example:123";
        when(jdbcTemplate.queryForObject(
                anyString(),
                any(Object[].class),
                any(int[].class),
                any(RowMapper.class))
        ).thenThrow(new EmptyResultDataAccessException(1));

        Validator actualValidator = validatorCacheDao.getFromCache(didURI);

        assertThat(actualValidator).isNull();
    }

    @Test
    void addCacheShouldCallJdbc() {
        Validator validator = new Validator("did:example:123", "publicKey", Instant.now());

        validatorCacheDao.addToCache(validator);

        verify(jdbcTemplate).update(
                eq("insert into validatorcache(diduri, publickey, expirationtime) values(?, ?, ?)"),
                eq(validator.getDidURI()),
                eq(validator.getPublicKey()),
                eq(Timestamp.from(validator.getExpirationDate()))
        );
    }

    @Test
    void removeFromCacheShouldCallJdbc() {
        String didURI = "did:example:123";

        validatorCacheDao.removeFromCache(didURI);

        verify(jdbcTemplate).update(
                eq("delete from validatorcache where diduri = ?"),
                eq(didURI)
        );
    }

    @Test
    void expireValidatorsShouldReturnValidResponse() {
        when(jdbcTemplate.update(anyString(), any(Timestamp.class))).thenReturn(3);

        int expiredCount = validatorCacheDao.expireValidators();

        assertThat(expiredCount).isEqualTo(3);
    }
}
package eu.xfsc.fc.core.dao.impl;

import eu.xfsc.fc.core.service.schemastore.SchemaRecord;
import eu.xfsc.fc.core.service.schemastore.SchemaStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.sql.Types.VARCHAR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SchemaDaoImplTest {

    @Mock
    private JdbcTemplate jdbc;

    @InjectMocks
    private SchemaDaoImpl schemaDaoImpl;

    @Test
    void selectSchemasByTerm() {

        var term = "test term";
        var expected = new HashMap<Integer, Collection<String>>();

        when(jdbc.query(
                eq("select s.type, s.schemaId from schemafiles s join schematerms t on t.schemaid = s.schemaid where t.term = ?"),
                eq(new Object[] {term}),
                eq(new int[] {VARCHAR}),
                any(SchemaDaoImpl.SchemaAggregateExtractor.class)))
                .thenReturn(expected);

        var result = schemaDaoImpl.selectSchemasByTerm(term);

        assertNotNull(result);
        assertEquals(expected, result);
    }

    @Test
    void insertReturnsTrueTest() {
        var id = "testId";
        var terms = Set.of("term1", "term2");
        var srMock = mock(SchemaRecord.class);
        var expected = Map.of("one", 1, "two", 2);

        when(srMock.getId()).thenReturn(id);
        when(srMock.terms()).thenReturn(terms);
        when(srMock.nameHash()).thenReturn("hash");
        when(srMock.type()).thenReturn(SchemaStore.SchemaType.ONTOLOGY);
        when(srMock.updateTime()).thenReturn(Instant.now());
        when(srMock.content()).thenReturn("content");

        when(jdbc.queryForObject(
                anyString(),
                any(Object[].class),
                any(int[].class),
                any(SchemaDaoImpl.SchemaAggregateMapper.class)))
                .thenReturn(expected);

        var result = schemaDaoImpl.insert(srMock);
        assertTrue(result);
    }

    @Test
    void insertReturnsFalseTest() {
        var id = "testId";
        var terms = Set.of("term1", "term2");
        var srMock = mock(SchemaRecord.class);
        Map<String, Integer> expected = Collections.emptyMap();

        when(srMock.getId()).thenReturn(id);
        when(srMock.terms()).thenReturn(terms);
        when(srMock.nameHash()).thenReturn("hash");
        when(srMock.type()).thenReturn(SchemaStore.SchemaType.ONTOLOGY);
        when(srMock.updateTime()).thenReturn(Instant.now());
        when(srMock.content()).thenReturn("content");

        when(jdbc.queryForObject(
                anyString(),
                any(Object[].class),
                any(int[].class),
                any(SchemaDaoImpl.SchemaAggregateMapper.class)))
                .thenReturn(expected);

        var result = schemaDaoImpl.insert(srMock);
        assertFalse(result);
    }

    @Test
    void updateTest() {

        var testId = "testId";
        var content = "content";
        var terms = List.of("term1", "term2");
        var expected = Map.of("one", 1, "two", 2);

        when(jdbc.update("delete from schematerms where schemaid = ?", testId))
                .thenReturn(1);
        when(jdbc.queryForObject(
                anyString(),
                any(Object[].class),
                any(int[].class),
                any(SchemaDaoImpl.SchemaAggregateMapper.class)))
                .thenReturn(expected);

        var result = schemaDaoImpl.update(testId, content, terms);
        assertEquals(expected.size(), result);
    }

    @Test
    void deleteTest() {
        var schemaId = "testId";
        var expected = Integer.valueOf(8);

        when(jdbc.queryForObject(
                eq("delete from schemafiles where schemaid = ? returning type"),
                eq(new Object[] {schemaId}),
                eq(new int[] {VARCHAR}),
                eq(Integer.class)))
                .thenReturn(expected);

        var result = schemaDaoImpl.delete(schemaId);

        assertNotNull(result);
        assertEquals(expected, result);
    }

    @Test
    void deleteReturnsNullWhenExceptionTest() {
        var schemaId = "testId";

        when(jdbc.queryForObject(
                eq("delete from schemafiles where schemaid = ? returning type"),
                any(Object[].class),
                any(int[].class),
                eq(Integer.class)))
                .thenThrow(new EmptyResultDataAccessException("Test exception thrown", 1));

        var result = schemaDaoImpl.delete(schemaId);

       assertNull(result);
    }
}

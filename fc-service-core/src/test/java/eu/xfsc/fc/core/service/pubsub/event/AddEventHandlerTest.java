package eu.xfsc.fc.core.service.pubsub.event;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.xfsc.fc.api.generated.model.SelfDescriptionResult;
import eu.xfsc.fc.client.SelfDescriptionClient;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.pojo.SelfDescriptionMetadata;
import eu.xfsc.fc.core.pojo.VerificationResult;
import eu.xfsc.fc.core.pojo.VerificationResultOffering;
import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import eu.xfsc.fc.core.service.verification.VerificationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class AddEventHandlerTest {

    @Mock
    private SelfDescriptionStore sdStore;

    @Mock
    private VerificationService verificationService;

    @Mock
    private Map<String, SelfDescriptionClient> sdClients;

    @Mock
    private ObjectMapper jsonMapper;

    @InjectMocks
    private AddEventHandler addEventHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testHandleWithDataString() throws Exception {
        // Arrange
        Map<String, Object> params = new HashMap<>();
        String dataStr = "{\"verificationResult\":\"{\\\"id\\\":\\\"123\\\",\\\"issuer\\\":\\\"issuer1\\\"}\"}";
        params.put("data", dataStr);
        String hash = "someHash";
        params.put("hash", hash);

        VerificationResult mockVerificationResult = mock(VerificationResult.class);
        when(mockVerificationResult.getId()).thenReturn("123");
        when(mockVerificationResult.getIssuer()).thenReturn("issuer1");

        Map<String, Object> parsedData = new HashMap<>();
        parsedData.put("verificationResult", "{\"id\":\"123\",\"issuer\":\"issuer1\"}");
        parsedData.put("content", "someContent");

        // Mock jsonMapper.readValue for both data and verificationResult
        when(jsonMapper.readValue(eq(dataStr), any(TypeReference.class))).thenReturn(parsedData);
        when(jsonMapper.readValue(eq("{\"id\":\"123\",\"issuer\":\"issuer1\"}"), eq(VerificationResult.class)))
            .thenReturn(mockVerificationResult); // Mock verificationResult parsing

        addEventHandler.handle(params);

        ArgumentCaptor<SelfDescriptionMetadata> metadataCaptor = ArgumentCaptor.forClass(SelfDescriptionMetadata.class);
        ArgumentCaptor<VerificationResult> verificationCaptor = ArgumentCaptor.forClass(VerificationResult.class);
        verify(sdStore).storeSelfDescription(metadataCaptor.capture(), verificationCaptor.capture());
        VerificationResult capturedVerificationResult = verificationCaptor.getValue();
        assertEquals("123", capturedVerificationResult.getId());
        assertEquals("issuer1", capturedVerificationResult.getIssuer());
    }

    @Test
    void testHandleWithoutDataString() throws Exception {
        Map<String, Object> params = new HashMap<>();
        String source = "source1";
        String hash = "hash123";
        params.put("source", source);
        params.put("hash", hash);

        SelfDescriptionClient mockClient = mock(SelfDescriptionClient.class);
        when(sdClients.computeIfAbsent(eq(source), any())).thenReturn(mockClient);

        SelfDescriptionResult mockSdResult = mock(SelfDescriptionResult.class);
        when(mockSdResult.getContent()).thenReturn("selfDescriptionContent");
        when(mockClient.getSelfDescriptionByHash(eq(hash), eq(false), eq(true)))
                .thenReturn(mockSdResult);

        VerificationResultOffering mockVerificationResultOffering = mock(VerificationResultOffering.class);
        when(mockVerificationResultOffering.getId()).thenReturn("123");
        when(mockVerificationResultOffering.getIssuer()).thenReturn("issuer1");

        when(verificationService.verifyOfferingSelfDescription(any(ContentAccessorDirect.class)))
                .thenReturn(mockVerificationResultOffering);

        addEventHandler.handle(params);

        ArgumentCaptor<SelfDescriptionMetadata> metadataCaptor = ArgumentCaptor.forClass(SelfDescriptionMetadata.class);
        ArgumentCaptor<VerificationResultOffering> verificationCaptor = ArgumentCaptor.forClass(VerificationResultOffering.class);
        verify(sdStore).storeSelfDescription(metadataCaptor.capture(), verificationCaptor.capture());
        VerificationResultOffering capturedVerificationResult = verificationCaptor.getValue();
        assertEquals("123", capturedVerificationResult.getId());
        assertEquals("issuer1", capturedVerificationResult.getIssuer());
    }
}
package eu.xfsc.fc.core.service.pubsub.event;

import eu.xfsc.fc.core.service.sdstore.SelfDescriptionStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class DeleteEventHandlerTest {

    @Mock
    private SelfDescriptionStore sdStore;

    @InjectMocks
    private DeleteEventHandler deleteEventHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this); // Initializes mocks
    }

    @Test
    void testHandleWithValidHash() {
        String hash = "someHash";
        Map<String, Object> params = new HashMap<>();
        params.put("hash", hash);
        deleteEventHandler.handle(params);
        ArgumentCaptor<String> hashCaptor = ArgumentCaptor.forClass(String.class);
        verify(sdStore, times(1)).deleteSelfDescription(hashCaptor.capture());
        assertEquals(hash, hashCaptor.getValue());
    }
}

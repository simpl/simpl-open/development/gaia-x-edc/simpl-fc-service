package eu.xfsc.fc.core.service.schemastore;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.xfsc.fc.core.dao.SchemaDao;
import eu.xfsc.fc.core.exception.ClientException;
import eu.xfsc.fc.core.exception.ConflictException;
import eu.xfsc.fc.core.exception.NotFoundException;
import eu.xfsc.fc.core.exception.ServerException;
import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.service.filestore.FileStore;
import eu.xfsc.fc.core.util.TestUtil;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.vocabulary.RDF;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DuplicateKeyException;

import static eu.xfsc.fc.core.util.SchemaStoreUtils.addUrlsToSetForGivenNodeFromModel;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SchemaStoreImplTest {

    @Mock
    private FileStore fileStore;

    @Mock
    private SchemaDao dao;

    @InjectMocks
    private SchemaStoreImpl schemaStore;

    @Test
    void initializeDefaultSchemasShouldAddSchemasFromDirectory() throws IOException {
        when(dao.getSchemaCount()).thenReturn(0);
        when(dao.insert(any())).thenReturn(true);

        int addedSchemas = schemaStore.initializeDefaultSchemas();

        assertThat(addedSchemas).isEqualTo(4);
        verify(dao, times(2)).getSchemaCount();
    }

    @Test
    void initializeDefaultSchemasShouldNotAddSchemasFromDirWhenCount1() {
        when(dao.getSchemaCount()).thenReturn(1);

        int addedSchemas = schemaStore.initializeDefaultSchemas();

        assertThat(addedSchemas).isZero();
        verify(dao, never()).insert(any());
    }

    @Test
    void analyzeSchemaShouldReturnValidResult() {
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        SchemaAnalysisResult result = schemaStore.analyzeSchema(contentGraph);

        assertThat(result.isValid()).isTrue();
        assertThat(result.getSchemaType()).isEqualTo(SchemaStore.SchemaType.ONTOLOGY);
    }

    @Test
    void analyzeSchemaShouldReturnInvalidResult() {
        String pathGraph = "Schema-Tests/invalid-schemaShape.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        SchemaAnalysisResult result = schemaStore.analyzeSchema(contentGraph);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getErrorMessage()).isNotEmpty().isEqualTo("Schema type not supported");
    }

    @Test
    void addSchemaShouldStoreValidSchema() {
        String pathGraph = "Schema-Tests/validShacl.jsonld";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        when(dao.insert(any())).thenReturn(true);

        String schemaId = schemaStore.addSchema(contentGraph);

        assertThat(schemaId).isNotNull();
    }

    @Test
    void addSchemaShouldThrowConflictExceptionWithMessageAboutRedefiningTerms() {
        String pathGraph = "Schema-Tests/validSkosWith2Urls.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);
        doThrow(new DuplicateKeyException("schematerms_pkey")).when(dao).insert(any());

        assertThatThrownBy(() -> schemaStore.addSchema(contentGraph))
                .isInstanceOf(ConflictException.class)
                .hasMessageContaining("Schema redefines existing terms");
    }

    @Test
    void addSchemaShouldThrowConflictExceptionWithMessageAboutAlreadyExists() {
        String pathGraph = "Schema-Tests/validOntology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);
        doThrow(new DuplicateKeyException("schemafiles_pkey")).when(dao).insert(any(SchemaRecord.class));

        assertThatThrownBy(() -> schemaStore.addSchema(contentGraph))
                .isInstanceOf(ConflictException.class)
                .hasMessageContaining(" already exists.");
    }

    @Test
    void addSchemaShouldThrowServerExceptionWithMessageDbError() {
        String pathGraph = "Schema-Tests/validShacl.rdfxml";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);
        when(dao.insert(any())).thenReturn(false);

        assertThatThrownBy(() -> schemaStore.addSchema(contentGraph))
                .isInstanceOf(ServerException.class)
                .hasMessageContaining("DB error, schema not inserted");
    }

    @Test
    void addSchemaShouldThrowServerExceptionWithMessageAboutAUnexpectedError() {
        String pathGraph = "Schema-Tests/validOntology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);
        doThrow(new DuplicateKeyException("Unexpected error")).when(dao).insert(any(SchemaRecord.class));

        assertThatThrownBy(() -> schemaStore.addSchema(contentGraph))
                .isInstanceOf(ServerException.class)
                .hasMessageContaining("Unexpected error");
    }

    @Test
    void deleteSchemaShouldNotThrowException() {
        when(dao.delete(anyString())).thenReturn(1);

        assertThatCode(() -> schemaStore.deleteSchema("someId"))
                .doesNotThrowAnyException();

    }

    @Test
    void deleteSchemaShouldThrowNotFoundException() {
        when(dao.delete(anyString())).thenReturn(null);

        assertThatThrownBy(() -> schemaStore.deleteSchema("nonExistingId"))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("Schema with id");
    }

    @Test
    void clearShouldNotThrowException() throws IOException {
        when(dao.deleteAll()).thenReturn(10);

        assertThatCode(() -> schemaStore.clear())
                .doesNotThrowAnyException();

        verify(fileStore).clearStorage();
    }


    @Test
    void isSchemaTypeShouldReturnTrue() {
        String pathGraph = "Schema-Tests/validOntology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        boolean isType = schemaStore.isSchemaType(contentGraph, SchemaStore.SchemaType.ONTOLOGY);

        assertThat(isType).isTrue();
    }

    @Test
    void isSchemaTypeShouldReturnFalse() {
        String pathGraph = "Schema-Tests/validOntology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        boolean isType = schemaStore.isSchemaType(contentGraph, SchemaStore.SchemaType.SHAPE);

        assertThat(isType).isFalse();
    }

    @Test
    void getCompositeSchemaShouldCreateNewOneIfAbsent() throws IOException {
        String pathGraph = "Schema-Tests/validOntology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);
        when(dao.selectSchemas())
                .thenReturn(Map.of(0, List.of("lala")));
        final var schemaRecord = new SchemaRecord("schemaId", "hash",
                SchemaStore.SchemaType.ONTOLOGY, contentGraph.getContentAsString(), Set.of("dudu"));
        when(dao.select(anyString()))
                .thenReturn(schemaRecord);
        when(fileStore.readFile(any())).thenReturn(new ContentAccessorDirect("content"));

        ContentAccessor result = schemaStore.getCompositeSchema(SchemaStore.SchemaType.ONTOLOGY);


        assertThat(result).isNotNull();
        verify(fileStore).replaceFile(any(), any());
    }

    @Test
    void getSchemaShouldReturnContentAccessorWhenFound() {
        SchemaRecord schemaRecord = new SchemaRecord("id", "hash", SchemaStore.SchemaType.ONTOLOGY,
                "content", Collections.emptySet());
        when(dao.select("id")).thenReturn(schemaRecord);

        ContentAccessor result = schemaStore.getSchema("id");

        assertThat(result.getContentAsString()).isEqualTo("content");
    }

    @Test
    void getSchemaShouldThrowExceptionWhenSchemaNotFound() {
        when(dao.select("nonExistingId")).thenReturn(null);

        assertThatThrownBy(() -> schemaStore.getSchema("nonExistingId"))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("was not found");
    }

    @Test
    void testAddExtractedUrls() {
        Model mockModel = mock(Model.class);
        RDFNode mockNode = mock(RDFNode.class);
        ResIterator mockResIterator = mock(ResIterator.class);
        org.apache.jena.rdf.model.Resource mockResource1 = mock(org.apache.jena.rdf.model.Resource.class);
        org.apache.jena.rdf.model.Resource mockResource2 = mock(org.apache.jena.rdf.model.Resource.class);

        Set<String> extractedSet = new HashSet<>();

        when(mockModel.listResourcesWithProperty(RDF.type, mockNode)).thenReturn(mockResIterator);
        when(mockResIterator.hasNext()).thenReturn(true, true, false); // Two resources, then end
        when(mockResIterator.nextResource()).thenReturn(mockResource1, mockResource2);
        when(mockResource1.getURI()).thenReturn("http://example.org/resource1");
        when(mockResource2.getURI()).thenReturn("http://example.org/resource2");

        // Act
        addUrlsToSetForGivenNodeFromModel(mockModel, mockNode, extractedSet);

        // Assert
        assertThat(extractedSet).containsExactlyInAnyOrder(
                "http://example.org/resource1",
                "http://example.org/resource2"
        );

        verify(mockModel, times(1)).listResourcesWithProperty(RDF.type, mockNode);
        verify(mockResIterator, times(3)).hasNext(); // Called 3 times: true, true, false
        verify(mockResIterator, times(2)).nextResource(); // Called 2 times for the resources
        verify(mockResource1, times(1)).getURI();
        verify(mockResource2, times(1)).getURI();
    }

    @Test
    void verifySchemaShouldReturnTrueForValidSchema() {
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        final var result = schemaStore.verifySchema(contentGraph);

        assertThat(result).isTrue();
    }

    @Test
    void updateSchemaShouldNotThrowException() {
        String identifier = "http://w3id.org/gaia-x/core#";
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        when(dao.update(any(), any(), any())).thenReturn(1);

        assertThatCode(() -> schemaStore.updateSchema(identifier, contentGraph))
                .doesNotThrowAnyException();
    }

    @Test
    void updateSchemaShouldThrowClientException() {
        String identifier = "identifierMismatch";
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);


        assertThatThrownBy(() -> schemaStore.updateSchema(identifier, contentGraph))
                .isInstanceOf(ClientException.class)
                .hasMessageContaining("Given schema does not have the same Identifier as the old");

        verify(dao, never()).update(any(), any(), any());
    }

    @Test
    void updateSchemaShouldThrowNotFoundException() {
        String identifier = "http://w3id.org/gaia-x/core#";
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        when(dao.update(any(), any(), any())).thenReturn(0);



        assertThatThrownBy(() -> schemaStore.updateSchema(identifier, contentGraph))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("was not found");
    }

    @Test
    void updateSchemaShouldThrowConflictException() {
        String identifier = "http://w3id.org/gaia-x/core#";
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        doThrow(new DuplicateKeyException("schematerms_pkey")).when(dao).update(any(), any(), any());

        assertThatThrownBy(() -> schemaStore.updateSchema(identifier, contentGraph))
                .isInstanceOf(ConflictException.class)
                .hasMessageContaining("Schema redefines existing terms");
    }

    @Test
    void updateSchemaShouldThrowServerException() {
        String identifier = "http://w3id.org/gaia-x/core#";
        String pathGraph = "Schema-Tests/gax-test-ontology.ttl";
        ContentAccessor contentGraph = TestUtil.getAccessor(getClass(), pathGraph);

        doThrow(new DuplicateKeyException("some server exception")).when(dao).update(any(), any(), any());

        assertThatThrownBy(() -> schemaStore.updateSchema(identifier, contentGraph))
                .isInstanceOf(ServerException.class)
                .hasMessageContaining("some server exception");
    }

    @Test
    void testGetSchemasForTerm_ReturnsExpectedMap() {
        // Arrange
        String term = "exampleTerm";
        Map<Integer, Collection<String>> daoResult = new HashMap<>();
        daoResult.put(SchemaStore.SchemaType.ONTOLOGY.ordinal(), Arrays.asList("schema1", "schema2"));
        daoResult.put(SchemaStore.SchemaType.SHAPE.ordinal(), Arrays.asList("schema3"));

        when(dao.selectSchemasByTerm(term)).thenReturn(daoResult);

        // Act
        final var result = schemaStore.getSchemasForTerm(term);

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).hasSize(2);
        assertThat(result).containsKeys(SchemaStore.SchemaType.ONTOLOGY, SchemaStore.SchemaType.SHAPE);
        assertThat(result.get(SchemaStore.SchemaType.ONTOLOGY)).containsExactly("schema1", "schema2");
        assertThat(result.get(SchemaStore.SchemaType.SHAPE)).containsExactly("schema3");

        verify(dao, times(1)).selectSchemasByTerm(term);
    }
}

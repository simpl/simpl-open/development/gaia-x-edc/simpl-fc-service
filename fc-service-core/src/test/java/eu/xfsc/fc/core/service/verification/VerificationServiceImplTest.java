package eu.xfsc.fc.core.service.verification;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;

import com.apicatalog.jsonld.loader.DocumentLoader;
import eu.xfsc.fc.core.dao.ValidatorCacheDao;
import eu.xfsc.fc.core.exception.VerificationException;
import eu.xfsc.fc.core.pojo.ContentAccessorDirect;
import eu.xfsc.fc.core.service.filestore.CacheFileStore;
import eu.xfsc.fc.core.service.schemastore.SchemaStore;
import eu.xfsc.fc.core.service.verification.ontology.OntologyVerificationService;
import eu.xfsc.fc.core.service.verification.signature.SignatureVerifier;
import eu.xfsc.fc.core.util.ClaimValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static eu.xfsc.fc.core.util.TestUtil.getAccessor;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VerificationServiceImplTest {

    @InjectMocks
    private VerificationServiceImpl verificationService;

    @Mock
    private SchemaStore schemaStore;

    @Mock
    private SignatureVerifier signVerifier;

    @Mock
    private CacheFileStore fileStore;

    @Mock
    private DocumentLoader documentLoader;

    @Mock
    private ValidatorCacheDao validatorCache;

    @Mock
    private OntologyVerificationService ontologyVerificationService;

    @BeforeEach
    void setUp() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Set up reflection for private fields
        ReflectionTestUtils.setField(verificationService, "requireVP", false);
        ReflectionTestUtils.setField(verificationService, "verifySemantics", true);
        ReflectionTestUtils.setField(verificationService, "verifySchema", true);
        ReflectionTestUtils.setField(verificationService, "verifyVPSignature", false);
        ReflectionTestUtils.setField(verificationService, "verifyVCSignature", false);
        ReflectionTestUtils.setField(verificationService, "dropValidators", false);
        ReflectionTestUtils.setField(verificationService, "participantType", "PARTICIPANT");
        ReflectionTestUtils.setField(verificationService, "serviceOfferingType", "SERVICE_OFFERING");
        ReflectionTestUtils.setField(verificationService, "resourceType", "RESOURCE");
        ReflectionTestUtils.setField(verificationService, "trustAnchorAddr", "http://example.com");
        ReflectionTestUtils.setField(verificationService, "httpTimeout", 5000);
        ReflectionTestUtils.setField(verificationService, "validatorExpiration", Duration.ofDays(1));

        // Initialize post-construct fields
        Method method = verificationService.getClass().getDeclaredMethod("initializeTrustFrameworkBaseClasses");
        method.setAccessible(true);
        method.invoke(verificationService);
    }

    @Test
    void verifyOfferingSelfDescriptionShouldNotThrowException() throws VerificationException {
        try (var claimValidator = mockStatic(ClaimValidator.class)) {
            claimValidator.when(() -> ClaimValidator.getSubjectType(any(), any(), any(), any()))
                    .thenReturn(TrustFrameworkBase.SERVICE_OFFERING);

        final var contentAccessorForSignedSd =  getAccessor("VerificationService/syntax/input.vc.jsonld");

        assertThatCode(() -> verificationService.verifyOfferingSelfDescription(contentAccessorForSignedSd))
                .doesNotThrowAnyException();
        verify(ontologyVerificationService).verifyPayloadAgainstOntology(any());
        verify(schemaStore, times(2)).getCompositeSchema(any());
        }
    }

    @Test
    void verifyOfferingSelfDescriptionShouldThrowVerificationExceptionWhenBaseClassUnknown() throws VerificationException {
        try (var claimValidator = mockStatic(ClaimValidator.class)) {
            claimValidator.when(() -> ClaimValidator.getSubjectType(any(), any(), any(), any()))
                    .thenReturn(TrustFrameworkBase.UNKNOWN);

            final var contentAccessor = getAccessor("VerificationService/syntax/input.vc.jsonld");

            assertThatThrownBy(() -> verificationService.verifyOfferingSelfDescription(contentAccessor))
                    .isInstanceOf(VerificationException.class)
                    .hasMessageContaining("Semantic Error: no proper CredentialSubject found");
        }
    }

    @Test
    void verifyOfferingSelfDescriptionShouldThrowVerificationExceptionWhenExpectedClassOtherThanServiceOffering() throws VerificationException {
        try (var claimValidator = mockStatic(ClaimValidator.class)) {
            claimValidator.when(() -> ClaimValidator.getSubjectType(any(), any(), any(), any()))
                    .thenReturn(TrustFrameworkBase.PARTICIPANT);

            final var contentAccessor = getAccessor("VerificationService/syntax/input.vc.jsonld");

            assertThatThrownBy(() -> verificationService.verifyOfferingSelfDescription(contentAccessor))
                    .isInstanceOf(VerificationException.class)
                    .hasMessageContaining("Semantic error: expected SD of type");
        }
    }

    @Test
    void verifySelfDescriptionAgainstSchemaShouldReturnResult() throws VerificationException {
        try (var claimValidator = mockStatic(ClaimValidator.class)) {
            claimValidator.when(() -> ClaimValidator.validateClaimsBySchema(any(), any(), any()))
                    .thenReturn("report");

            final var payload = getAccessor("VerificationService/syntax/input.vc.jsonld");
            when(schemaStore.getCompositeSchema(any()))
                    .thenReturn(new ContentAccessorDirect("lalala"));

            final var result = verificationService.verifySelfDescriptionAgainstSchema(payload, null);

            assertThat(result.isConforming()).isFalse();
            assertThat(result.getValidationReport()).isEqualTo("report");
        }
    }
}
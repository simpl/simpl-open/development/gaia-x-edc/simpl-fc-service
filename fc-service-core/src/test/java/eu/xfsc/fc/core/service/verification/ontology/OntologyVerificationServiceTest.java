package eu.xfsc.fc.core.service.verification.ontology;

import eu.xfsc.fc.core.exception.VerificationException;
import eu.xfsc.fc.core.pojo.ContentAccessor;
import eu.xfsc.fc.core.service.schemastore.SchemaStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OntologyVerificationServiceTest {

    @Mock
    private SchemaStore schemaStore;

    @InjectMocks
    private OntologyVerificationService ontologyVerificationService;

    @Test
    void verifyPayloadAgainstOntologyValidPayloadNoException() {
        final var payload = OntologyDataFactory.VALID_PAYLOAD;

        final var mockContentAccessor = mock(ContentAccessor.class);
        when(schemaStore.getCompositeSchema(SchemaStore.SchemaType.ONTOLOGY)).thenReturn(mockContentAccessor);
        when(mockContentAccessor.getContentAsString()).thenReturn(OntologyDataFactory.ONTOLOGY);

       assertThatCode(() -> ontologyVerificationService.verifyPayloadAgainstOntology(payload))
               .doesNotThrowAnyException();
    }

    @Test
    void verifyPayloadAgainstOntologyInvalidPayloadInferenceModelValidationError()  {
        final var mockContentAccessor = mock(ContentAccessor.class);
        when(schemaStore.getCompositeSchema(SchemaStore.SchemaType.ONTOLOGY)).thenReturn(mockContentAccessor);
        when(mockContentAccessor.getContentAsString()).thenReturn(OntologyDataFactory.ONTOLOGY);

        assertThatThrownBy(() -> ontologyVerificationService.verifyPayloadAgainstOntology(OntologyDataFactory.INVALID_PAYLOAD_RANGE_MISMATCH))
                .isInstanceOf(VerificationException.class)
                .hasMessageContaining("Inference model validation error");
    }

    @Test
    void validateDataAgainstOntologyDomainMismatchError() {
        final var mockContentAccessor = mock(ContentAccessor.class);
        when(schemaStore.getCompositeSchema(SchemaStore.SchemaType.ONTOLOGY)).thenReturn(mockContentAccessor);
        when(mockContentAccessor.getContentAsString()).thenReturn(OntologyDataFactory.ONTOLOGY);

        assertThatThrownBy(() -> ontologyVerificationService.verifyPayloadAgainstOntology(OntologyDataFactory.INVALID_PAYLOAD_DOMAIN_MISMATCH))
                .isInstanceOf(VerificationException.class)
                .hasMessageContaining("Domain validation error: dataObject:");
    }
}